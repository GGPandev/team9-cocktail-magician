import multer from 'multer';

const mimeTypeExtensions = new Map([
    ['image/jpeg', 'jpg'],
    ['image/jpg', 'jpg'],
    ['image/png', 'png']
]);

const rnd = () => Math.round(Math.random() * 1E9);
const fileFilter = (req, file, cb) => {
    if (mimeTypeExtensions.has(file.mimetype)) {
        cb(null, true);
    } else {
        cb({
            fileError: true,
            message: `Unsupported file type: ${file.mimetype}`
        }, false);
    }
};
const getFileName = (req, file, cb) => {
    const [fileName] = file.originalname.split('.');
    const ext = mimeTypeExtensions.get(file.mimetype);
    const newName = `${fileName}_${rnd()}${rnd()}.${ext}`;

    cb(null, newName)
}

export const cocktailFiles = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/cocktails')
        },
        filename: getFileName
    }),
    fileFilter: fileFilter
});

export const barFiles = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/bars')
        },
        filename: getFileName
    }),
    fileFilter: fileFilter
});
