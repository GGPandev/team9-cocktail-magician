export const roles = Object.freeze({
    CRAWLER: 'crawler',
    MAGICIAN: 'magician'
});
