import express from 'express';
import asyncHandler from 'express-async-handler';
import { insert, select, tables, update } from '../data/database.js';
import { roles } from '../data/roles.js';
import { auth, validator, isInRole, barCocktailInterceptor } from '../middlewares.js';
import schema from '../validations.js';
import { barFiles } from '../data/file-storage.js';

const getBar = (key, value) =>
    select(`SELECT name, address, phone FROM ${tables.bars} WHERE ${key} = ?`, [value]);
const getBarImages = (barId) =>
    select(`SELECT id, url, isCover, isVisible FROM ${tables.barImages} WHERE barId = ?`, [barId]);
const getBarCocktails = (barId) =>
    select(`SELECT c.id, c.name, c.imageUrl,
                (SELECT group_concat(i.name, ', ') FROM ${tables.ingredients} as i 
                INNER JOIN ${tables.cocktailsIngredients} as ci 
                ON ci.ingredientId = i.id AND ci.cocktailId = c.id) as ingredients 
            FROM ${tables.cocktails} as c
            INNER JOIN ${tables.barsCocktails} as bc
                ON c.id = bc.cocktailId
            WHERE bc.barId = ?`, [barId]);
const getCocktailsByIngredients = async (ingredients) => {
    if (typeof ingredients === 'string') {
        ingredients = [ingredients]
    }

    if (ingredients && ingredients.length > 0) {
        const available = await select(
            `SELECT c.id, c.name, i.name as ingredientName from ${tables.cocktails} as c
             INNER JOIN ${tables.cocktailsIngredients} as ci ON c.id = ci.cocktailId
             INNER jOIN ${tables.ingredients} as i ON i.id = ci.ingredientId 
             WHERE i.name in (${ingredients.map(i => `'${i}'`).join(', ')})`);

        return [available, ingredients];
    }

    return [null, null];
}

const bars = express.Router();

bars.get('/', asyncHandler(async (req, res) => {
    const barName = req.query.name
    const bars = await select(`
        SELECT b.id, name, address, phone, bi.url,
            (SELECT avg(rating) from ${tables.barReviews} where barId = b.id) as rating,
            (SELECT count(id) from ${tables.barReviews} where barId = b.id) as reviewsCount
        FROM ${tables.bars} as b
        LEFT JOIN ${tables.barImages} as bi on bi.barId = b.id AND bi.isCover = 1
        ${barName ? `WHERE name LIKE '%${barName}%'` : ``}`);

    res.send(bars);
}));

bars.get('/cocktail-ingredients', asyncHandler(async (req, res) => {
    const [searchedCocktails, ingredientNames] = await getCocktailsByIngredients(req.query.values);

    if (!searchedCocktails) {
        return res.send([])
    }

    const results = await Promise.all(ingredientNames.map(async ingredientName => {
        const cocktails = searchedCocktails.filter(c => c.ingredientName === ingredientName);

        const bars = await select(`
            SELECT b.id as barId, b.name as barName, b.phone, b.address, 
                   c.id as cocktailId, c.name as cocktailName, c.imageUrl as cocktailImage
            FROM ${tables.bars} as b
            INNER JOIN ${tables.barsCocktails} as bc ON b.id = bc.barId
            INNER JOIN ${tables.cocktails} as c ON c.id = bc.cocktailId
            WHERE c.id in (${cocktails.map(c => `${c.id}`).join(', ')})`);

        return { ingredientName, bars }
    }))

    res.send(results);
}));

bars.get('/:id', asyncHandler(async (req, res) => {
    const [bar] = await select(`
        SELECT id, name, address, phone, 
            (SELECT avg(rating) from ${tables.barReviews} where barId = b.id) as rating,
            (SELECT count(id) from ${tables.barReviews} where barId = b.id) as reviewsCount
        FROM ${tables.bars} as b
        WHERE id = ?
    `, [req.params.id]);

    if (!bar) {
        res.status(404).send({ message: `Bar ${req.params.id} not found` });
    } else {
        res.send({
            ...bar,
            images: await getBarImages(bar.id),
            cocktails: await getBarCocktails(bar.id)
        });
    }
}));

bars.post(
    '/',
    auth,
    isInRole(roles.MAGICIAN),
    barFiles.array('images', 20),
    validator(schema.createBar),
    asyncHandler(async (req, res) => {
        if (req.files.length === 0) {
            return res.status(400).send({ message: 'At least one image is required' });
        }

        const { name, address, phone } = req.body;
        const [bar] = await getBar('name', name);

        if (bar) {
            return res.status(409).send({ message: 'Bar name taken' });
        }

        const { recordId: id } = await insert(
            `INSERT INTO ${tables.bars}(name, address, phone) VALUES(?, ?, ?)`,
            [name, address, phone]);

        await insert(
            `INSERT INTO ${tables.barImages}(url, barId)
            VALUES${req.files.map(({ filename }) => `('/bars/${filename}', ${id})`).join(',')};`
        );

        res.send({ id, name, address, phone, images: await getBarImages(id) });
    }));

bars.put(
    '/:id',
    auth,
    isInRole(roles.MAGICIAN),
    validator(schema.updateBar),
    asyncHandler(async (req, res) => {
        const { name, address, phone } = req.body;
        const [bar] = await getBar('id', req.params.id);

        if (!bar) {
            return res.status(404).send({ message: 'Bar does not exist' });
        }

        if (name && (await getBar('name', name)).length > 0) {
            return res.status(409).send({ message: 'Bar name taken' });
        }

        await update(
            `UPDATE ${tables.bars} SET name = ?, address = ?, phone = ? WHERE id = ?`,
            [name ?? bar.name, address ?? bar.address, phone ?? bar.phone, req.params.id]
        );

        res.send({
            id: req.params.id,
            name: name ?? bar.name,
            address: address ?? bar.address,
            phone: phone ?? bar.phone
        });
    }));

bars.get(
    '/:id/reviews',
    asyncHandler(async (req, res) => {
        const reviews = await select(`
            SELECT r.id, r.text, r.rating, u.id as userId, u.username, u.displayName
            FROM ${tables.barReviews} as r
                INNER JOIN ${tables.users} as u
                ON r.userId = u.id
            WHERE r.barId = ?
        `, [req.params.id]);

        res.send(reviews.map(r => ({
            id: r.id,
            text: r.text,
            rating: r.rating,
            author: {
                id: r.userId,
                username: r.username,
                displayName: r.displayName
            }
        })));
    }));

bars.post(
    '/:id/reviews',
    auth,
    validator(schema.createReview),
    asyncHandler(async (req, res) => {
        const { text, rating } = req.body;
        const [bar] = await getBar('id', req.params.id);

        if (!bar) {
            return res.status(404).send({ message: 'Bar does not exist' });
        }

        const [review] = await select(
            `SELECT text FROM ${tables.barReviews} WHERE userId = ? AND barId = ?`,
            [req.user.id, req.params.id]);

        if (review) {
            return res.status(409).send({ message: 'Already reviewed' });
        }

        const { recordId } = await insert(
            `INSERT INTO ${tables.barReviews}(text, rating, barId, userId) VALUES(?, ?, ?, ?)`,
            [text, rating, req.params.id, req.user.id]);

        res.send({ id: recordId, text, rating });
    }));

bars.put(
    '/:barId/images/:imageId/cover',
    auth,
    isInRole(roles.MAGICIAN),
    asyncHandler(async (req, res) => {
        const { barId, imageId } = req.params;

        const barImages = await getBarImages(barId);
        const targetImage = barImages.find(img => img.id === +imageId)
        if (!targetImage) {
            return res.status(404).send({ message: `Bar ${barId} has no image ${imageId}` });
        }

        const prevCover = barImages.find(img => !!img.isCover);
        if (prevCover && prevCover.id === imageId) {
            return res.send(prevCover);
        }

        if (prevCover) {
            await update(`UPDATE ${tables.barImages} SET isCover = 0 WHERE id = ?`, [prevCover.id]);
        }

        await update(`UPDATE ${tables.barImages} SET isCover = 1, isVisible = 1 WHERE id = ?`, [imageId]);

        res.send({ ...targetImage, isCover: 1 });
    }));

bars.put(
    '/:barId/images/:imageId',
    auth,
    isInRole(roles.MAGICIAN),
    validator(schema.hideImageActions),
    asyncHandler(async (req, res) => {
        const { barId, imageId } = req.params;
        const action = req.body.action;

        const barImages = await getBarImages(barId);
        const targetImage = barImages.find(img => img.id === +imageId)
        if (!targetImage) {
            return res.status(404).send({ message: `Bar ${barId} has no image ${imageId}` });
        }

        if (targetImage.isCover && action === 'hide') {
            return res.status(400).send({ message: `Bar covers must remain visible` });
        }

        if ((action === 'hide' && targetImage.isHidden === 1) ||
            (action === 'show' && targetImage.isHidden === 0)) {
            return res.send(targetImage);
        }

        const flag = (action === 'show') ? 1 : 0;
        await update(`UPDATE ${tables.barImages} SET isVisible = ? WHERE id = ?`, [flag, imageId]);

        res.send({ ...targetImage, isVisible: flag });
    }));

bars.post(
    '/:barId/images',
    auth,
    isInRole(roles.MAGICIAN),
    barFiles.single('image'),
    asyncHandler(async (req, res) => {
        const { barId } = req.params;
        if (!req.file) {
            return res.status(400).send({ message: 'Image is required' });
        }

        if ((await getBar('id', barId)).length === 0) {
            return res.status(404).send({ message: `Bar ${barId} not found` });
        }

        await insert(`INSERT INTO ${tables.barImages}(url, barId) VALUES('/bars/${req.file.filename}', ?);`, [barId]);

        res.send(await getBarImages(barId));
    }));

bars.put(
    '/:barId/cocktails/:cocktailId',
    auth,
    isInRole(roles.MAGICIAN),
    barCocktailInterceptor,
    asyncHandler(async (req, res) => {
        if (!req.customData.barCocktail) {
            await insert(
                `INSERT INTO ${tables.barsCocktails}(cocktailId,barId) VALUES(?,?)`,
                [req.params.cocktailId, req.params.barId]
            );
        }

        res.sendStatus(204);
    }));

bars.delete(
    '/:barId/cocktails/:cocktailId',
    auth,
    isInRole(roles.MAGICIAN),
    barCocktailInterceptor,
    asyncHandler(async (req, res) => {
        if (req.customData.barCocktail) {
            await update(
                `DELETE FROM ${tables.barsCocktails} WHERE cocktailId = ? AND barId = ?`,
                [req.params.cocktailId, req.params.barId]);
        }

        res.sendStatus(204);
    }));

export default bars;
