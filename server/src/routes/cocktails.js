import express from 'express';
import asyncHandler from 'express-async-handler';
import { insert, select, tables, update } from '../data/database.js';
import { roles } from '../data/roles.js';
import { auth, handleIngredients, isInRole, validator } from '../middlewares.js';
import schema from '../validations.js';
import { cocktailFiles } from '../data/file-storage.js';

const getCocktailDetails = async (id = null) => {
    const cocktails = await select(
        `SELECT 
            c.id, 
            c.name,
            c.imageUrl,
            i.id as ingrId,
            i.name as ingrName,
            (SELECT COUNT(*) from ${tables.favouriteCocktails} as fc
             WHERE fc.cocktailId = c.id) as favouriteCount 
         FROM ${tables.cocktails} as c
         INNER JOIN ${tables.cocktailsIngredients} as c_i
         ON c_i.cocktailId = c.id
         INNER JOIN ${tables.ingredients} as i
         ON c_i.ingredientId = i.id
         ${id ? 'WHERE c.id = ?;' : ';'}`, id ? [id] : []);

    const aggregated = cocktails.reduce((groups, c) => {
        if (!groups.has(c.id)) {
            groups.set(c.id, {
                id: c.id,
                name: c.name,
                imageUrl: c.imageUrl,
                favouriteCount: c.favouriteCount,
                ingredients: []
            })
        }

        groups
            .get(c.id)
            .ingredients
            .push({ id: c.ingrId, name: c.ingrName });

        return groups
    }, new Map());

    return [...aggregated.values()];
}

const cocktails = express.Router();

cocktails.get('/', asyncHandler(async (req, res) => {
    res.send(await getCocktailDetails());
}));

cocktails.get('/:id', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const [cocktail] = await getCocktailDetails(id);

    if (!cocktail) {
        return res.status(404).send({ message: `Cocktail ${id} not found` });
    }

    res.send({
        ...cocktail,
        availableIn: await select(`SELECT b.id, b.name 
                                   FROM ${tables.bars} as b INNER JOIN ${tables.barsCocktails} as br
                                   ON b.id = br.barId AND br.cocktailId = ?`, [id]),
        favouritedBy: await select(`SELECT u.id, u.username, u.displayName 
                                    FROM ${tables.users} as u INNER JOIN ${tables.favouriteCocktails} as fc
                                    ON u.id = fc.userId AND fc.cocktailId = ?`, [id]),
    });
}));

cocktails.post('/',
    auth,
    isInRole(roles.MAGICIAN),
    cocktailFiles.single('image'),
    validator(schema.createCocktail),
    handleIngredients,
    asyncHandler(async (req, res) => {
        if (!req.file) {
            return res.status(400).send({ message: 'Cocktail image is required' })
        }

        const { name } = req.body;
        const { ingredients } = req.customData;

        const [cocktail] = await select(`SELECT id, name FROM ${tables.cocktails} where name = ?`, [name]);

        if (cocktail) {
            return res.status(409).send({ message: `Cocktail ${name} already exists` });
        }

        const imageUrl = `/cocktails/${req.file.filename}`;
        const { recordId } = await insert(
            `INSERT INTO ${tables.cocktails}(name, imageUrl) VALUES(?, ?)`,
            [req.body.name, imageUrl]);

        await insert(
            `INSERT INTO ${tables.cocktailsIngredients}(cocktailId,ingredientId) 
            VALUES${ingredients.map(({ id }) => `(${recordId}, ${id})`).join(',')};`
        );

        res.send({ id: recordId, name, imageUrl, ingredients });
    }));

cocktails.put('/:id',
    auth,
    isInRole(roles.MAGICIAN),
    cocktailFiles.single('image'),
    validator(schema.updateCocktail),
    asyncHandler(async (req, res) => {
        const { name } = req.body;

        if (name && (await select(`SELECT id FROM ${tables.cocktails} where name = ?`, [name])).length > 0) {
            return res.status(409).send({ message: `Cocktail ${name} already exists` });
        }

        const [cocktail] = await select(`SELECT id, name, imageUrl FROM ${tables.cocktails} where id = ?`, [req.params.id]);

        if (!cocktail) {
            return res.status(404).send({ message: `Cocktail ${req.params.id} does not exist` });
        }

        const newName = name ?? cocktail.name;
        const newImageUrl = req.file ? `/cocktails/${req.file.filename}` : cocktail.imageUrl;

        await update(`UPDATE ${tables.cocktails} SET name = ?, imageUrl = ? WHERE id = ?`,
            [newName, newImageUrl, req.params.id]);

        res.send({ id: cocktail.id, name: newName, imageUrl: newImageUrl });
    }));

cocktails.put('/:id/ingredients',
    auth,
    isInRole(roles.MAGICIAN),
    validator(schema.addIngredients),
    handleIngredients,
    asyncHandler(async (req, res) => {
        const { ingredients } = req.customData;
        const [cocktail] = await getCocktailDetails(req.params.id);

        if (!cocktail) {
            return res.status(404).send({ message: `Cocktail ${req.params.id} does not exist` });
        }

        const newIngredients = ingredients.filter(i => !cocktail.ingredients.some(ci => ci.id === i.id));

        if (newIngredients.length === 0) {
            return res.send(cocktail);
        }

        await insert(
            `INSERT INTO ${tables.cocktailsIngredients}(cocktailId,ingredientId) 
            VALUES${newIngredients.map(({ id }) => `(${cocktail.id}, ${id})`).join(',')};`
        );

        res.send({
            ...cocktail,
            ingredients: [...cocktail.ingredients, ...newIngredients]
        });
    }));

cocktails.delete('/:id/ingredients',
    auth,
    isInRole(roles.MAGICIAN),
    validator(schema.removeIngredients),
    handleIngredients,
    asyncHandler(async (req, res) => {
        const { ingredients } = req.customData;
        const [cocktail] = await getCocktailDetails(req.params.id);

        if (!cocktail) {
            return res.status(404).send({ message: `Cocktail ${req.params.id} does not exist` });
        }

        const deletedIngredients = cocktail.ingredients
            .filter(ci => ingredients.some(i => i.id === ci.id));

        if (deletedIngredients.length === cocktail.ingredients.length) {
            return res.status(400).send({ message: 'At least one ingredient should survive' });
        }

        await update(`DELETE FROM ${tables.cocktailsIngredients} 
                      WHERE ingredientId IN (${deletedIngredients.map(i => `${i.id}`).join(', ')})
                      AND cocktailId = ${cocktail.id}`);

        res.send((await getCocktailDetails(req.params.id))[0]);
    }));

export default cocktails;
