import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import express from 'express';
import asyncHandler from 'express-async-handler';
import { secret } from '../data/secret.js';
import { insert, select, tables, update } from '../data/database.js';
import { roles } from '../data/roles.js';
import { auth, validator, favouriteCocktailInterceptor } from '../middlewares.js';
import schema from '../validations.js';

const getUser = (key, value) => select(`
    SELECT 
        id, 
        username, 
        password, 
        role, 
        displayName 
    FROM ${tables.users} WHERE ${key} = ?`, [value]);

const withoutPassword = user => ({
    id: user.id,
    username: user.username,
    role: user.role,
    displayName: user.displayName
});

const getUserDetails = async (user) => {
    return {
        ...withoutPassword(user),
        favouriteCocktails: await select(
            `SELECT id, name FROM ${tables.cocktails} as c
         INNER JOIN ${tables.favouriteCocktails} as fc ON c.id = fc.cocktailId
         WHERE fc.userId = ?`, [user.id]),
        reviewedBars: await select(
            `SELECT b.id, b.name, 
                br.text, br.rating as myRating,
                (SELECT avg(r.rating) FROM ${tables.barReviews} as r WHERE r.barId = b.id) as avgRating 
        FROM ${tables.bars} as b
        INNER JOIN ${tables.barReviews} as br ON b.id = br.barId
        WHERE br.userId = ?`, [user.id])
    };
}

const users = express.Router();

users.post('/', validator(schema.register), asyncHandler(async (req, res) => {
    const { username, displayName, password } = req.body;

    const [user] = await getUser('username', username);

    if (user) {
        return res.status(409).send({ message: 'Username taken' });
    }

    const hash = await bcrypt.hash(password, 10);
    const { recordId } = await insert(
        `INSERT INTO ${tables.users}(username, displayName, password) VALUES(?, ?, ?)`,
        [username, displayName, hash]);

    res.send({ id: recordId, username, role: roles.CRAWLER, displayName: (displayName ? displayName : '' )});
}));

users.post('/login', validator(schema.login), asyncHandler(async (req, res) => {
    const { username, password } = req.body;

    const [user] = await getUser('username', username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
        return res.status(400).send({ message: 'Incorrect username or password' });
    }

    res.send({ token: jwt.sign({ sub: user.id, username:user.username, role: user.role }, secret, { expiresIn: 240 * 3600 }) });
}));

users.get('/me', auth, asyncHandler(async (req, res) => {
    const [user] = await getUser('id', req.user.id);
    res.send(await getUserDetails(user));
}));

users.get('/:id', asyncHandler(async (req, res) => {
    const [user] = await getUser('id', req.params.id);
    if (!user) {
        return res.status(404).send({ message: `User ${req.params.id} not found!` });
    }

    res.send(await getUserDetails(user))
}));

users.put('/', auth, validator(schema.updateUser), asyncHandler(async (req, res) => {
    const [user] = await getUser('id', req.user.id);
    const { displayName } = req.body;

    if (!displayName) return res.send(withoutPassword(user));

    await update(
        `UPDATE ${tables.users} SET displayName = ? WHERE id = ?`,
        [displayName, req.user.id]
    );

    return res.send(withoutPassword({ ...user, displayName }));
}));

users.put(
    '/cocktails/:cocktailId',
    auth,
    favouriteCocktailInterceptor,
    asyncHandler(async (req, res) => {
        if (!req.customData.favourite) {
            await insert(
                `INSERT INTO ${tables.favouriteCocktails}(cocktailId,userId) VALUES(?,?)`,
                [req.params.cocktailId, req.user.id]);
        }

        res.sendStatus(204);
    }));

users.delete(
    '/cocktails/:cocktailId',
    auth,
    favouriteCocktailInterceptor,
    asyncHandler(async (req, res) => {
        if (req.customData.favourite) {
            await update(
                `DELETE FROM ${tables.favouriteCocktails} WHERE cocktailId = ? AND userId = ?`,
                [req.params.cocktailId, req.user.id]);
        }

        res.sendStatus(204);
    }));

export default users;
