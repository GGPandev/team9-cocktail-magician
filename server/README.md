## Cocktail Magician Backend

### Setup
```
npm install
npm start
```

- runs on http://localhost:5000

### Documentation

Magician-level user (admin) is available out of the box:
```json
{
    "username":"admin",
    "password":"admin
}
``` 
To view all available endpoints:
- Go to `/docs` in this repo and import the postman collection
- You can explore the documentation for each endpoint:
    - if the `View Documentation` option is disabled, you must sign-in to Postman

    ![img](./docs/docs.jpg)
