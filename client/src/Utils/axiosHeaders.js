import { getToken } from '../Providers/AuthContext';

export const authorizationHeader = { Authorization: `Bearer ${getToken()}`}


export const contentTypeHeader = {
   'Content-Type': 'application/json' 
};
