import { END_INDEX, START_INDEX } from "../Constants/constants"

export const filteredCountCocktails = (currentPage, setFilteredCocktails, cocktails) => {
  if (currentPage > 1) {
    setFilteredCocktails(cocktails.slice((currentPage * END_INDEX / currentPage), currentPage * END_INDEX))
  } else {
    setFilteredCocktails(cocktails.slice(START_INDEX, currentPage * END_INDEX))  
  }
}