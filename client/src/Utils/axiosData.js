
export const registerData = (name, password, nickname) => {
  return {
    username: name,
    password: password,
    displayName: nickname ? nickname : '',
  } 
};

export const loginData = (name, password) => {
  return {
    username: name,
    password: password,
  } 
};