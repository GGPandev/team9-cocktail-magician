export const API_URL = 'http://localhost:5000/api/v1';

// export const IMG_BAR_PATH = 'http://localhost:5000/api/v1/';
export const IMG_COCKTAIL_PATH = 'http://localhost:5000/public/cocktails/';

export const START_INDEX = 0;
export const END_INDEX = 6;

export const COCKTAIL_NAME_MIN_LENGTH = 2;
export const COCKTAIL_NAME_MAX_LENGTH = 30;
export const USERNAME_MIN_LENGTH = 3;
export const USERNAME_MAX_LENGTH = 30;
export const PASSWORD_MIN_LENGTH = 5;
export const PASSWORD_MAX_LENGTH = 30;

export const NICKNAME_MIN_LENGTH = 3;
export const NICKNAME_MAX_LENGTH = 30;



