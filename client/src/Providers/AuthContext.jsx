import jwt from 'jsonwebtoken';
import { createContext } from 'react';

export const AuthContext = createContext({
  token: '',
  user: {
    id: 0,
    username: '',
    role: '',
  },
  isLoggedIn: false,
  setAuth: () => {},
});

export const getToken = () => localStorage.getItem('token') || '';

export const getUser = () => {
  try {
    return jwt.decode(getToken());
  } catch {
    return null;
  }
};
