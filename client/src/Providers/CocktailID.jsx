import { createContext } from "react";

const CocktailID = createContext({
  cocktailId: 0,
  setCocktailID: () => {},
});

export default CocktailID;