import { TextField } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { API_URL, PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH, USERNAME_MAX_LENGTH, USERNAME_MIN_LENGTH } from '../../Constants/constants';
import { registerUser } from '../../Requests/requests';
import { registerData } from '../../Utils/axiosData';
import { contentTypeHeader } from '../../Utils/axiosHeaders';
import './Register.scss';

const Register = () => {
  const navigate = useNavigate();
  const [username, setCurrentUsername] = useState('');
  const [password, setCurrentPassword] = useState('');
  const [nickname, setNickname] = useState('');
  const [isRegisterBtnClicked, setIsRegisterBtnClicked] = useState(false);
  const [registerError, setRegisterError] = useState('');
  const [usernameError, setUsernameError] = useState('')
  const [passwordError ,setPasswordError] = useState('')

  useEffect(() => {
    if (isRegisterBtnClicked) {
      if (username.length >= USERNAME_MIN_LENGTH && username.length <= USERNAME_MAX_LENGTH && 
        password.length >= PASSWORD_MIN_LENGTH && password.length <= PASSWORD_MIN_LENGTH) {

        registerUser(username, password, nickname, setRegisterError, navigate);

        setIsRegisterBtnClicked(false);
        setPasswordError('');
        setUsernameError('');
      }

      if (username.length < USERNAME_MIN_LENGTH || username.length > USERNAME_MAX_LENGTH) {
        setUsernameError(`Must be between ${USERNAME_MIN_LENGTH} and ${USERNAME_MAX_LENGTH} characters long.`)
        setIsRegisterBtnClicked(false);
      } 
      if (password.length < PASSWORD_MIN_LENGTH || password.length > PASSWORD_MAX_LENGTH) {
        setPasswordError(`Must be between ${PASSWORD_MIN_LENGTH} and ${PASSWORD_MAX_LENGTH} characters long.`)
        setIsRegisterBtnClicked(false);
      }

      setIsRegisterBtnClicked(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isRegisterBtnClicked]);

  useEffect(() => {
    if (username.length >= USERNAME_MIN_LENGTH) {
      setUsernameError('');
    }
    if (password.length >= PASSWORD_MIN_LENGTH) {
      setPasswordError('');
    }
  }, [password, username, isRegisterBtnClicked]);

  return (
    <div className="register">
      <h1>Register</h1>
      {registerError && <p className="error-msg">{registerError}</p>}
      <TextField style={{ background: 'white', marginBottom: '20px', width: '270px'}}
        variant="filled"
        id="username"
        type="text"
        label="Username"
        value={username}
        autoComplete="off"
        onChange={(e) => setCurrentUsername(e.target.value)}
      />
      {usernameError && <p className="error-msg">{usernameError}</p>}
      <TextField style={{ background: 'white', marginBottom: '20px', width: '270px'}}
        variant="filled"
        id="nickname"
        type="text"
        label="Nickname"
        value={nickname}
        autoComplete="off"
        onChange={(e) => setNickname(e.target.value)}
      />
      <TextField style={{ background: 'white',  width: '270px'}}
        variant="filled"
        id="password"
        type="password"
        label="Password"
        value={password}
        autoComplete="off"
        onChange={(e) => setCurrentPassword(e.target.value)}
      />
       {passwordError && <p className="error-msg">{passwordError}</p>}
      <button type="submit" onClick={() => setIsRegisterBtnClicked(true)}>
        Register
      </button>
      <br />
    </div>
  );
};

export default Register;
