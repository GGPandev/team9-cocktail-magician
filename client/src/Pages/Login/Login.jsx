import { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../Providers/AuthContext';
import './Login.scss';
import { PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH, USERNAME_MAX_LENGTH, USERNAME_MIN_LENGTH } from '../../Constants/constants';
import { Link, useNavigate } from 'react-router-dom';
import Input from '../../Components/Input/Input';
import { loginUser } from '../../Requests/requests';

const Login = () => {
  const navigate = useNavigate();
  const { setAuth } = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoginBtnClicked, setIsLoginBtnClicked] = useState(false);
  const [loginError, setLoginError] = useState('');
  const [usernameError, setUsernameError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  useEffect(() => {
    if (isLoginBtnClicked) {
      if (username.length >= USERNAME_MIN_LENGTH && username.length <= USERNAME_MAX_LENGTH &&
        password.length >= PASSWORD_MIN_LENGTH && password.length <= PASSWORD_MAX_LENGTH) {
        
          loginUser(username, password, setAuth, setLoginError, navigate);

        setIsLoginBtnClicked(false);
        setPasswordError('');
        setUsernameError('');
      }

      if (username.length < USERNAME_MIN_LENGTH || username.length > USERNAME_MAX_LENGTH) {
        setUsernameError(`Must be between ${USERNAME_MIN_LENGTH} and ${USERNAME_MAX_LENGTH} characters long.`);
        setIsLoginBtnClicked(false);
      }
      if (password.length < PASSWORD_MIN_LENGTH || password.length > PASSWORD_MAX_LENGTH) {
        setPasswordError(`Must be between ${PASSWORD_MIN_LENGTH} and ${PASSWORD_MAX_LENGTH} characters long.`);
      }
      setIsLoginBtnClicked(false);
    }
  }, [isLoginBtnClicked, password, username, setAuth, navigate]);

  useEffect(() => {
    if (username.length >= USERNAME_MIN_LENGTH) {
      setUsernameError('');
    }
    if (password.length >= PASSWORD_MIN_LENGTH) {
      setPasswordError('');
    }
  }, [password, username, isLoginBtnClicked]);

  return (
    <div className="login">
      <h1>Login</h1>
      {loginError && <p className="error-msg">{loginError}</p>}
      <Input
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        label="Username"
        id="username"
      />
      {usernameError && <p className="error-msg">{usernameError}</p>}
      <Input
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        label="Password"
        id="password"
      />
      {passwordError && <p className="error-msg">{passwordError}</p>}

      <button type="submit" onClick={() => setIsLoginBtnClicked(true)}>
        Login
      </button>
      <br />

      <h4>
        Don`t have an account ? <Link to="/register"> Register now</Link>
      </h4>
    </div>
  );
};

export default Login;
