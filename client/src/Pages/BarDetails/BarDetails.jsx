import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { API_URL } from '../../Constants/constants';
import AddBarReview from '../../Components/AddBarReview/AddBarReview';
import BarCocktailBox from '../../Components/BarCocktailBox/BarCocktailBox';
import BarReviewBox from '../../Components/BarReviewBox/BarReviewBox';
import EditBarInfo from '../../Components/EditBarInfo/EditBarInfo';
import { AuthContext } from '../../Providers/AuthContext';
import CallIcon from '@mui/icons-material/Call';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import { Rating } from '@mui/material';
import './BarDetails.scss';
import SetPhotoCoverButton from '../../Components/SetPhotoCoverButton/SetPhotoCoverButton';
import UploadImageForm from '../../Components/UploadImageForm/UploadImageForm';

const BarDetails = () => {
  // Variables:
  const barId = sessionStorage.getItem('barId');
  // States:
  const [barObj, setBarObj] = useState({});
  const [barReviewsArr, setBarReviewArr] = useState([]);
  const [allCocktailsArr, setAllCocktailsArr] = useState([]);
  const [unlistedCocktailsArr, setUnlistedCocktailsArr] = useState([]);
  const [isDeleteBtnClicked, setIsDeleteBtnClicked] = useState(false);
  const [isAddBtnClicked, setIsAddBtnClicked] = useState(false);
  const [isBarReviewSent, setIsBarReviewSent] = useState(false);
  const [isEditedBarInfoSent, setIsEditedBarInfoSent] = useState(false);
  const [isBarReviewedByUser, setIsBarReviewedByUser] = useState(false);
  const [isCoverPhotoChanged, setIsCoverPhotoChanged] = useState(false);
  const [isImageUploaded, setIsImageUploaded] = useState(false);
  // Contexts:
  const { auth } = useContext(AuthContext);

  // Fetching the Bar Details Object:
  useEffect(() => {
    axios.get(API_URL + `/bars/${barId}`).then((res) => setBarObj(res.data));

    setIsAddBtnClicked(false);
    setIsDeleteBtnClicked(false);
    setIsEditedBarInfoSent(false);
    setIsCoverPhotoChanged(false);
    setIsImageUploaded(false);
  }, [
    isAddBtnClicked,
    isDeleteBtnClicked,
    isEditedBarInfoSent,
    isCoverPhotoChanged,
    isImageUploaded,
  ]);

  // Fetching the Bar Reviews Object:
  useEffect(() => {
    axios
      .get(API_URL + `/bars/${barId}/reviews`)
      .then((res) => setBarReviewArr(res.data));

    setIsBarReviewSent(false);
  }, [isBarReviewSent]);

  // Fetching the list with all available cocktails:
  useEffect(() => {
    axios
      .get(API_URL + `/cocktails`)
      .then((res) => setAllCocktailsArr(res.data));
  }, []);

  // Creating an Array with unavailable cocktails only:
  useEffect(() => {
    if (barObj.id && allCocktailsArr.length) {
      // console.log('Both Arrays are available and are being filtered...');

      const resultArr = allCocktailsArr.filter(
        ({ id: idArrOne }) =>
          !barObj.cocktails.some(({ id: idArrTwo }) => idArrOne === idArrTwo)
      );

      setUnlistedCocktailsArr(resultArr);
    }
  }, [barObj, allCocktailsArr]);

  // Check if the bar has already been reviewed by the current user:
  useEffect(() => {
    if (auth.isLoggedIn === true && barReviewsArr.length) {
      // console.log(barObj.id);
      // console.dir(barReviewsArr);
      // console.dir(auth);
      const filtered = barReviewsArr.filter(
        (bar) => bar.author.id === auth.user.sub
      );

      if (filtered.length) {
        setIsBarReviewedByUser(true);
        // console.log(filtered);
        // console.log(`Yeah! It's reviewed!!!`);
      }
    }
  }, [barObj, barReviewsArr]);

  return (
    <div>
      {barObj.id && (
        <div className="barDetails-header">
          <img
            src={
              API_URL +
              barObj.images.filter((image) => image.isCover === 1)[0].url
            }
            alt=""
          />
          <div className="barDetails-header-right">
            <div className="barDetails-header-right-text">
              <h1>{barObj.name}</h1>
              <h3>
                <CallIcon /> {barObj.phone}
              </h3>
              <h3>
                <LocationOnIcon /> {barObj.address}
              </h3>
              <h3>
                {auth.isLoggedIn && auth.user?.role === 'magician' ? (
                  <EditBarInfo
                    barId={barObj.id}
                    name={barObj.name}
                    phone={barObj.phone}
                    address={barObj.address}
                    onclick={() => setIsEditedBarInfoSent(true)}
                  />
                ) : null}
              </h3>
            </div>

            <div className="barDetails-header-right-rating-review">
              <div className="barRating">
                <Rating
                  name="rating"
                  precision={0.1}
                  value={barObj.rating}
                  readOnly
                  sx={{ fontSize: 40 }}
                />
              </div>
              <div className="textRating-addBarReview">
                <div className="textRating">
                  <p>/ Rating: {barObj.rating} /</p>
                </div>
                <div className="addBarReview">
                  {auth.isLoggedIn ? (
                    <AddBarReview
                      barId={barObj.id}
                      isBarReviewedByUser={isBarReviewedByUser}
                      onclick={() => setIsBarReviewSent(true)}
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {barObj.images && (
        <div className="bar-Details-title-gallery">
          <h1>Gallery:</h1>
          {auth.isLoggedIn && auth.user?.role === 'magician' ? (
            <UploadImageForm
              barId={barObj.id}
              onclick={() => setIsImageUploaded(true)}
            />
          ) : null}

          <div className="bar-Details-gallery">
            {barObj.images
              .filter((image) => image.isCover === 0)
              .map((image) => (
                <div key={'image' + image.id} className="bar-image">
                  <img src={API_URL + image.url} alt="" />

                  {auth.isLoggedIn && auth.user?.role === 'magician' ? (
                    <SetPhotoCoverButton
                      barId={barObj.id}
                      imageId={image.id}
                      onclick={() => setIsCoverPhotoChanged(true)}
                    />
                  ) : null}
                </div>
              ))}
          </div>
        </div>
      )}

      {barObj.id && barObj.cocktails.length ? (
        <div className="bar-Details-title-cocktails">
          <h1>Cocktails:</h1>
          <div className="bar-Details-cocktails">
            {barObj.cocktails
              .sort((a, b) => a.name.localeCompare(b.name))
              .map((cocktail) => (
                <BarCocktailBox
                  key={cocktail.id}
                  id={cocktail.id}
                  barId={barObj.id}
                  name={cocktail.name}
                  imageUrl={cocktail.imageUrl}
                  ingredients={cocktail.ingredients}
                  onclick={() => setIsDeleteBtnClicked(true)}
                  buttonType="delete"
                />
              ))}
          </div>
        </div>
      ) : null}

      {auth.isLoggedIn &&
      auth.user?.role === 'magician' &&
      unlistedCocktailsArr.length ? (
        <div className="bar-Details-title-cocktails">
          <h1>Add cocktails:</h1>
          <div className="bar-Details-cocktails">
            {unlistedCocktailsArr
              .sort((a, b) => a.name.localeCompare(b.name))
              .map((cocktail) => (
                <BarCocktailBox
                  key={cocktail.id}
                  id={cocktail.id}
                  barId={barObj.id}
                  name={cocktail.name}
                  imageUrl={cocktail.imageUrl}
                  ingredients={cocktail.ingredients
                    .map((ing) => ing.name)
                    .join(', ')}
                  onclick={() => setIsAddBtnClicked(true)}
                  buttonType="add"
                />
              ))}
          </div>
        </div>
      ) : null}

      {barReviewsArr.length ? (
        <div className="bar-Details-title-reviews">
          <h1>Reviews:</h1>
          <div className="bar-Details-reviews">
            {barReviewsArr.map((review) => (
              <BarReviewBox
                key={review.id}
                reviewText={review.text}
                reviewRating={review.rating}
                authorUsername={review.author.username}
                authorDisplayName={review.author.displayName}
                authorId={review.author.id}
              />
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default BarDetails;
