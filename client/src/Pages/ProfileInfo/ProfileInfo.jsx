import React, { useEffect, useState } from 'react'
import BarReviewBoxUserDetails from '../../Components/BarReviewBoxUserDetails/BarReviewBoxUserDetails';
import CocktailBoxSmall from '../../Components/CocktailBoxSmall/CocktailBoxSmall';
import { getAllCocktails } from '../../Requests/requests';
import { API_URL, NICKNAME_MAX_LENGTH, NICKNAME_MIN_LENGTH } from '../../Constants/constants';
import axios from 'axios';
import { authorizationHeader, contentTypeHeader } from '../../Utils/axiosHeaders';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import EditIcon from '@mui/icons-material/Edit';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@mui/material';
import { Box } from '@mui/system';


const ProfileInfo = () => {
  const [userObj, setUserObj] = useState({});
  const [allCocktailsArr, setAllCocktailsArr] = useState([]);
  const [detailedFavCocktails, setDetailedFavCocktails] = useState([]);
  const [openEdit, setOpenEdit] = useState(false);
  const [nickname, setNickname] = useState('');
  const [isSubmitEditNicknameBntClicked, setIsSubmitEditNicknameBntClicked] = useState(false);
  const [nicknameError, setNicknameError] = useState(false)

  const handleClickOpenEditCocktail = () => {
    setOpenEdit(true);
  };

  const handleClose = () => {
    setOpenEdit(false);
  };

  const handleSubmitName = (event) => {
    event.preventDefault();
    
    if (nickname.length < NICKNAME_MIN_LENGTH || nickname.length > NICKNAME_MAX_LENGTH) {
      setNicknameError(true)
    } else {
      
      setOpenEdit(false);
      setIsSubmitEditNicknameBntClicked(true)    
      setNicknameError(false)
    }
  };

  useEffect(() => {
    axios.get(API_URL + `/users/me`, {headers: authorizationHeader})
    .then((res) => setUserObj(res.data));
  }, [isSubmitEditNicknameBntClicked]);

  useEffect(() => {
    getAllCocktails(setAllCocktailsArr)
  }, []);

  useEffect(() => {
    if (userObj.favouriteCocktails && allCocktailsArr.length) {
      const resultArr = allCocktailsArr.filter(({ id: idArrOne }) =>
        userObj.favouriteCocktails.some(
          ({ id: idArrTwo }) => idArrOne === idArrTwo
        )
      );
      setDetailedFavCocktails(resultArr);
    }
  }, [userObj, allCocktailsArr]);

  useEffect(() => {
    if(isSubmitEditNicknameBntClicked) {
      const data = {
        displayName: nickname
      }
      axios.put(API_URL + '/users', data, { headers: { ...authorizationHeader, ...contentTypeHeader }})
      .catch(err => console.log(err.message))
      setIsSubmitEditNicknameBntClicked(false)
    }
  }, [isSubmitEditNicknameBntClicked, nickname])

  return (
    <div>
      {userObj.id && (
        <div className="user-Details-main">
          <div className="userDetails-userInfo">
            <div className="user-Details-icon">
              <AccountCircleIcon sx={{ fontSize: 60 }} />
            </div>
            <div style={{ display: "inline-block", width: "70%", textAlign: "left"}} className="user-Details-username">
              <h3 style={{ display: "inline-block", marginRight: "30px"}}>
                Username: {userObj.username}
              </h3>
              <h3 style={{ display: "inline-block"}}>
                Nickname: {userObj.displayName ? userObj.displayName : "No Nickname"}
              </h3>
              <EditIcon sx={{ color: 'white'}} onClick={handleClickOpenEditCocktail}/>
              <Dialog open={openEdit} onClose={handleClose}>
                <DialogTitle>Edit Nickname</DialogTitle>
                <DialogContent>
                  <Box component="form" onSubmit={handleSubmitName}>
                    <TextField
                      autoFocus
                      required={!nicknameError}
                      margin="dense"
                      id="name"
                      label='Change Your Nickname'
                      type="text"
                      fullWidth
                      name="name"
                      value={nickname}
                      onChange={(e) => setNickname(e.target.value)}
                      error={nicknameError}
                      helperText={nicknameError && `Cocktail Name must be between ${NICKNAME_MIN_LENGTH} and ${NICKNAME_MAX_LENGTH} symbols`}
                    /> 
                    <DialogActions>
                      <Button onClick={handleClose}>Cancel</Button>
                      <Button type="submit">Submit</Button>
                    </DialogActions>
                  </Box>
                </DialogContent>
              </Dialog>
            </div>
          </div>

          <div className="userDetails-favCocktails-title">
            <h2>Favorite Cocktails:</h2>
            {detailedFavCocktails.length && (
              <div className="userDetails-favCocktails">
                {detailedFavCocktails
                  .sort((a, b) => a.name.localeCompare(b.name))
                  .map((cocktail) => (
                    <CocktailBoxSmall
                      key={cocktail.id}
                      id={cocktail.id}
                      name={cocktail.name}
                      imageUrl={cocktail.imageUrl}
                    />
                  ))}
              </div>
            )}
          </div>

          <div className="userDetails-barsReviewed-title">
            <h2>Reviewed Bars:</h2>
            {userObj.id && userObj.reviewedBars.length && (
              <div className="userDetails-barsReviewed">
                {userObj.reviewedBars
                  .sort((a, b) => a.myRating - b.myRating)
                  .map((bar) => (
                    <BarReviewBoxUserDetails
                      key={bar.id}
                      barId={bar.id}
                      barName={bar.name}
                      barReview={bar.text}
                      userRating={bar.myRating}
                      averageRating={bar.avgRating}
                    />
                  ))}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default ProfileInfo
