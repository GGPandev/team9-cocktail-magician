import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import './AddBar.scss';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { TextField } from '@mui/material';
import Button from '@mui/material/Button';
import BackupIcon from '@mui/icons-material/Backup';
import { API_URL } from '../../Constants/constants';
import { getToken } from '../../Providers/AuthContext';
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const initialValues = {
  name: '',
  phone: '',
  address: '',
};

const AddBar = () => {
  const navigate = useNavigate();
  const [data, setData] = useState(initialValues);
  const [files, setFiles] = useState(null);
  const [barId, setBarId] = useState(0);
  const [imageForCoverId, setImageForCoverId] = useState(0);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
    // console.log(values);
  };

  const handleFiles = (e) => {
    console.log(e.target.files, '$$$$');
    console.log(e.target.files[0], '$$$$');
    // console.log([...e.target.files], '$$$$');
    // const filesArr = [...e.target.files];
    // setData({ ...data, ['images']: e.target.files[0] });
    setFiles(e.target.files);
  };

  const submitFormHandler = (event) => {
    event.preventDefault();

    const formData = new FormData();

    for (let key in data) {
      formData.append(key, data[key]);
    }

    for (let key in files) {
      formData.append('images', files[key]);
    }

    console.log([...formData.entries()]);

    axios({
      url: `${API_URL}/bars`,
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      data: formData,
    })
      .then((res) => {
        setBarId(res.data.id);
        setImageForCoverId(res.data.images[0].id);
        console.log(res);
      })
      .then(() => navigate(`/bars`))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (barId > 0 && imageForCoverId > 0) {
      console.log(barId);
      console.log(imageForCoverId);

      axios({
        url: `${API_URL}/bars/${barId}/images/${imageForCoverId}/cover`,
        method: 'PUT',
        headers: {
          authorization: `Bearer ${getToken()}`,
        },
      });
    }
  }, [imageForCoverId]);

  return (
    <div className="addBar-container">
      <h1>Add Bar Form:</h1>

      <form className="addBar-form" onSubmit={submitFormHandler} action="">
        <TextField
          required
          name="name"
          variant="filled"
          label="Name"
          value={data.name}
          onChange={handleInputChange}
          fullWidth
          margin="dense"
          autoComplete="off"
        />
        <TextField
          required
          name="phone"
          variant="filled"
          label="Phone Number"
          value={data.phone}
          onChange={handleInputChange}
          fullWidth
          margin="dense"
          autoComplete="off"
        />
        <TextField
          required
          name="address"
          variant="filled"
          label="Address"
          value={data.address}
          onChange={handleInputChange}
          fullWidth
          margin="dense"
          autoComplete="off"
        />
        <br />
        <label htmlFor="cover-image">Cover Image:</label>
        <TextField
          id="cover-image"
          type="file"
          multiple="multiple"
          required
          name="images"
          variant="outlined"
          onChange={handleFiles}
          fullWidth
          margin="dense"
          inputProps={{ multiple: true }}
        />
        {/* <input
          type="file"
          multiple="multiple"
          margin="dense"
          variant="outlined"
        /> */}
        <br />
        <br />
        <Button type="submit" variant="contained" endIcon={<BackupIcon />}>
          Submit
        </Button>
      </form>
    </div>
  );
};

export default AddBar;
