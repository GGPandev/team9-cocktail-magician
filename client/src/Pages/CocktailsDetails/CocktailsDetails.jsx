import { Link } from 'react-router-dom';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import CocktailBarBox from '../../Components/CocktailBarBox/CocktailBarBox';
import CocktailBox from '../../Components/CocktailBox/CocktailBox';
import { API_URL } from '../../Constants/constants';
import { getCocktail } from '../../Requests/requests';
import './CocktailDetails.scss';
import { authorizationHeader } from '../../Utils/axiosHeaders';

const CocktailsDetails = () => {
  const cocktailId = sessionStorage.getItem('cocktailId');
  const [cocktail, setCocktail] = useState({});
  const [bars, setBars] = useState([]);
  const [allAvailableBarsInfo, setAllAvailableBarsInfo] = useState(null);
  const [isLikeStarBtnClicked, setIsLikeStarClicked] = useState(false);
  const [isUnLikeStarBtnClicked, setIsUnLikeStarClicked] = useState(false);

  const handleLikeCocktailClick = (id) => {
    sessionStorage.setItem('cocktailFavId', id);
    setIsLikeStarClicked(true);
  };

  const handleUnLikeCocktailClick = (id) => {
    sessionStorage.setItem('cocktailUnFavId', id);
    setIsUnLikeStarClicked(true);
  };

  useEffect(() => {
    if (cocktailId) {
      getCocktail(setCocktail, cocktailId);
    }
  }, [cocktailId, bars, isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  useEffect(() => {
    axios
      .get(API_URL + `/bars`)
      .then((res) => setBars(res.data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (isLikeStarBtnClicked) {
      fetch
        (API_URL + `/users/cocktails/${cocktailId}`, {
          method: 'PUT',
          headers: authorizationHeader,
        })
        .catch((err) => console.log(err.toJSON()));
        setIsLikeStarClicked(false)
    }

    if (isUnLikeStarBtnClicked) {
      fetch
        (API_URL + `/users/cocktails/${cocktailId}`, {
          method: 'DELETE',
          headers: authorizationHeader,
        })
        .catch((err) => console.log(err.toJSON()));
        setIsUnLikeStarClicked(false)
    }
  }, [cocktailId, isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  useEffect(() => {
    if (bars.length) {
      setAllAvailableBarsInfo(cocktail?.availableIn &&
        cocktail.availableIn.map((cb) => bars.filter((b) => b.id === cb.id))
      );
    }
  }, [bars, cocktail, isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  return (
    <div className="cocktail-details-container">
      <div className="cocktail-details-row1">
        <CocktailBox
          key={cocktail.id}
          id={cocktail.id}
          imageUrl={cocktail.imageUrl}
          name={cocktail.name}
          ingredients={cocktail.ingredients}
          favouriteCount={cocktail.favouriteCount}
          button={false}
          onclickStarToLike={handleLikeCocktailClick}
          onclickStarToUnlike={handleUnLikeCocktailClick}
          favoriteTo={cocktail.favouritedBy?.filter(
            (fav) => fav.id === cocktail.id
          )}
        />
      </div>

      <div className="cocktail-details-row2">
        <div className="cocktails-available-bars">
          <h2>Where to Drink that Cocktail:</h2>
          {allAvailableBarsInfo &&
            allAvailableBarsInfo.map((bar) =>
              bar.map((b) => (
                <CocktailBarBox
                  key={b.id}
                  id={b.id}
                  name={b.name}
                  address={b.address}
                  phone={b.phone}
                  url={b.url}
                  rating={b.rating}
                />
              ))
            )}
        </div>
        <div className="users-section">
          <h2>People Who like it</h2>
          {cocktail.favouritedBy &&
            cocktail.favouritedBy.map((fav) => (
              <h3 key={fav.id + 'u'}><Link to="/user-details" onClick={() => sessionStorage.setItem("userId", fav.id)}>## {  fav.displayName} ##</Link></h3>
            ))}
        </div>
      </div>
    </div>
  );
};

export default CocktailsDetails;
