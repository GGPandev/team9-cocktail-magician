import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../Constants/constants';
import './UserDetails.scss';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CocktailBoxSmall from '../../Components/CocktailBoxSmall/CocktailBoxSmall';
import BarReviewBoxUserDetails from '../../Components/BarReviewBoxUserDetails/BarReviewBoxUserDetails';

const UserDetails = () => {
  // Variables:
  const userId = sessionStorage.getItem('userId');
  // States:
  const [userObj, setUserObj] = useState({});
  const [allCocktailsArr, setAllCocktailsArr] = useState([]);
  const [detailedFavCocktails, setDetailedFavCocktails] = useState([]);

  // Fetching the User Details Object:
  useEffect(() => {
    axios.get(API_URL + `/users/${userId}`).then((res) => setUserObj(res.data));
  }, []);

  // Fetching the list with all available cocktails:
  useEffect(() => {
    axios
      .get(API_URL + `/cocktails`)
      .then((res) => setAllCocktailsArr(res.data));
  }, []);

  // Creating an Array with detailed favorite cocktails:
  useEffect(() => {
    if (userObj.favouriteCocktails && allCocktailsArr.length) {
      const resultArr = allCocktailsArr.filter(({ id: idArrOne }) =>
        userObj.favouriteCocktails.some(
          ({ id: idArrTwo }) => idArrOne === idArrTwo
        )
      );

      // console.log(resultArr);
      setDetailedFavCocktails(resultArr);
    }
  }, [userObj, allCocktailsArr]);

  return (
    <div>
      {userObj.id && (
        <div className="user-Details-main">
          <div className="userDetails-userInfo">
            <div className="user-Details-icon">
              <AccountCircleIcon sx={{ fontSize: 60 }} />
            </div>
            <div className="user-Details-username">
              <h3>
                {userObj.displayName ? userObj.displayName : userObj.username}
              </h3>
            </div>
          </div>

          <div className="userDetails-favCocktails-title">
            <h2>Favorite Cocktails:</h2>
            {detailedFavCocktails.length && (
              <div className="userDetails-favCocktails">
                {detailedFavCocktails
                  .sort((a, b) => a.name.localeCompare(b.name))
                  .map((cocktail) => (
                    <CocktailBoxSmall
                      key={cocktail.id}
                      id={cocktail.id}
                      name={cocktail.name}
                      imageUrl={cocktail.imageUrl}
                    />
                  ))}
              </div>
            )}
          </div>

          <div className="userDetails-barsReviewed-title">
            <h2>Reviewed Bars:</h2>
            {userObj.id && userObj.reviewedBars.length && (
              <div className="userDetails-barsReviewed">
                {userObj.reviewedBars
                  .sort((a, b) => a.myRating - b.myRating)
                  .map((bar) => (
                    <BarReviewBoxUserDetails
                      key={bar.id}
                      barId={bar.id}
                      barName={bar.name}
                      barReview={bar.text}
                      userRating={bar.myRating}
                      averageRating={bar.avgRating}
                    />
                  ))}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default UserDetails;
