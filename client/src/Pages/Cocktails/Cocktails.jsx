import { Pagination, Stack, Typography } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import CocktailBox from '../../Components/CocktailBox/CocktailBox';
import { API_URL, END_INDEX } from '../../Constants/constants';
import { getAllCocktails } from '../../Requests/requests';
import { authorizationHeader } from '../../Utils/axiosHeaders';
import { filteredCountCocktails } from '../../Utils/filteredCocktails';
import './Cocktails.scss';

const Cocktails = () => {
  const cocktailFavId = sessionStorage.getItem('cocktailFavId');
  const cocktailUnFavId = sessionStorage.getItem('cocktailUnFavId');
  const [cocktails, setCocktails] = useState([]);
  const [filteredCocktails, setFilteredCocktails] = useState([]);
  const [isLikeStarBtnClicked, setIsLikeStarClicked] = useState(false);
  const [isUnLikeStarBtnClicked, setIsUnLikeStarClicked] = useState(false);
  const [page, setPage] = useState(1);
  const [userInfo, setUserInfo] = useState({});

  const handleChange = (event, value) => {
    setPage(value);
  };

  const handleLikeCocktailClick = (id) => {
    sessionStorage.setItem('cocktailFavId', id);
    setIsLikeStarClicked(true);
  };

  const handleUnLikeCocktailClick = (id) => {
    sessionStorage.setItem('cocktailUnFavId', id);
    setIsUnLikeStarClicked(true);
  };
  useEffect(() => {
    getAllCocktails(setCocktails);
  }, [isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  useEffect(() => {
    filteredCountCocktails(page, setFilteredCocktails, cocktails);
  }, [page, cocktails, isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  useEffect(() => {
    if (isLikeStarBtnClicked) {
      // const data = {}
      // axios.put(API_URL + `/users/cocktails/${cocktailUnFavId}`, { headers: authorizationHeader })
      // .then(res => console.log(res))
      // .catch((err) => console.log(err.toJSON()));

      fetch
        (API_URL + `/users/cocktails/${cocktailFavId}`, {
          method: 'PUT',
          headers: authorizationHeader,
        })
        .catch((err) => console.log(err.toJSON()));
        setIsLikeStarClicked(false)
    }

    if (isUnLikeStarBtnClicked) {
      // const data = {}
      // axios.put(API_URL + `/users/cocktails/${cocktailUnFavId}`,  data,  { headers: authorizationHeader })
      // .catch((err) => console.log(err.toJSON()));

      fetch
        (API_URL + `/users/cocktails/${cocktailUnFavId}`, {
          method: 'DELETE',
          headers: authorizationHeader,
        })
        .catch((err) => console.log(err.toJSON()));
        setIsUnLikeStarClicked(false)
    }
  }, [cocktailFavId, cocktailUnFavId, isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  useEffect(() => {
    axios
      .get(API_URL + `/users/me`, { headers: authorizationHeader })
      .then((res) => setUserInfo(res.data))
      .catch((err) => console.log(err.message));
  }, [isLikeStarBtnClicked, isUnLikeStarBtnClicked]);

  return (
    <>
      <h1 style={{ color: 'white' }}>{cocktails.length} Cocktails found</h1>
      <div className="cocktails-page">
        {filteredCocktails.map((c) => (
          <CocktailBox
            key={c.id}
            id={c.id}
            name={c.name}
            imageUrl={c.imageUrl}
            ingredients={c.ingredients}
            favouriteCount={c.favouriteCount}
            button={true}
            onclick={() => sessionStorage.setItem('cocktailId', c.id)}
            onclickStarToLike={handleLikeCocktailClick}
            onclickStarToUnlike={handleUnLikeCocktailClick}

            favoriteTo={userInfo.favouriteCocktails?.filter(
              (fav) => fav.id === c.id
            )}
          />
        ))}
      </div>
      <Stack className="pagination" style={{ alignItems: 'center', color: 'white' }} spacing={2}>
        <Typography style={{ color: 'white' }}>Page: {page}</Typography>
        <Pagination
          hidePrevButton={page === 1}
          // hideNextButton={}
          size="large"
          shape="rounded"
          count={Math.ceil(cocktails.length / END_INDEX)}
          page={page}
          onChange={handleChange}
        />
      </Stack>
    </>
  );
};

export default Cocktails;
