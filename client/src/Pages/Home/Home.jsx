import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../images/CMM.png';
import Button from '@mui/material/Button';
import './Home.scss';

const Home = () => {
  return (
    <div className="homePage-container">
      <img
        className="home-logo"
        src={logo}
        alt="Cocktail Magician Manager Logo"
      />
      <div className="home-buttons">
        <Link to="/bars">
          <Button variant="contained" sx={{ fontSize: 20 }}>
            Bars ▶
          </Button>
        </Link>
        <Link to="/cocktails">
          <Button variant="contained" sx={{ fontSize: 20 }}>
            Cocktails ▶
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Home;
