import AddCircleIcon from '@mui/icons-material/AddCircle';
import EditIcon from '@mui/icons-material/Edit';
import { DataGrid } from '@mui/x-data-grid';
import React, { useEffect, useMemo, useState } from 'react';
import { API_URL, COCKTAIL_NAME_MAX_LENGTH, COCKTAIL_NAME_MIN_LENGTH } from '../../Constants/constants';
import './CocktailsTable.scss';
import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
} from '@mui/material';
import { Box } from '@mui/system';
import ListIngredients from '../../Components/ListAllIngredients/ListIngredients';
import { addNewCocktail, deleteIngredientFromCocktail, editCocktailImage, editCocktailName, getAllCocktails } from '../../Requests/requests';

const CocktailsTable = () => {
  const [cocktails, setCocktails] = useState([]);
  const [currentCocktailId, setCurrentCocktailId] = useState(0);
  const [ingredientToDelete, setIngredientToDelete] = useState('');
  const [cocktailName, setCocktailName] = useState('');
  const [isEditNameBntClicked, setIsEditNameBtnClicked] = useState(false);
  const [isSubmitCocktailClicked, setIsSubmitCocktailClicked] = useState(false);
  const [isDeleteIngredClicked, setIsDeleteIngredClicked] = useState(false);
  const [isAddIngredBtnClicked, setIsAddIngredBtnClicked] = useState(false);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openAddIngredients, setOpenAddIngredients] = useState(false);
  const [formData, setFormData] = useState({});
  const [fd, setFD] = useState({});
  const [cocktailNameError, setCocktailNameError] = useState(false)
  const [isEditImageBntClicked, setIsEditImageBtnClicked] = useState(false)
  const [isSubmitEditNameBntClicked, setIsSubmitEditNameBntClicked] = useState(false);
  const [isSubmitEditImageBntClicked, setIsSubmitEditImageBntClicked] = useState(false);

  const handleClickOpenAddCocktail = () => {
    setOpen(true);
  };

  const handleClickOpenEditCocktail = () => {
    setOpenEdit(true);
  };

  const handleClickOpenAddIngredients = () => {
    setOpenAddIngredients(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenEdit(false);
    setOpenAddIngredients(false)
    setIsEditNameBtnClicked(false);
    setIsEditImageBtnClicked(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (cocktailName.length < COCKTAIL_NAME_MIN_LENGTH || cocktailName.length > COCKTAIL_NAME_MAX_LENGTH) {
      setCocktailNameError(true)
    } else {
      setOpen(false);
      setCocktailNameError(false)
      setIsSubmitCocktailClicked(true);
  
      const currFormData = new FormData(event.target);
      const allData = [...currFormData.values()];
      const ingredients = allData[1]
      currFormData.delete('ingredients', allData[1])
      
      ingredients
        .toLocaleLowerCase()
        .split(',')
        .filter((i) => i.length > 2)
        .map((e) => e.trim())
        .forEach((i) => {
          currFormData.append('ingredients', i);
        });

      setFormData(currFormData);
    }
  };

  const handleSubmitName = (event) => {
    event.preventDefault();
    
    if (cocktailName.length < COCKTAIL_NAME_MIN_LENGTH || cocktailName.length > COCKTAIL_NAME_MAX_LENGTH) {
      setCocktailNameError(true)
    } else {
      setOpenEdit(false);
      setCocktailNameError(false)
      setIsSubmitEditNameBntClicked(true)    
    }
  };

  const handleSubmitImage = (event) => {
    event.preventDefault();
    setOpenEdit(false);
    setIsSubmitEditImageBntClicked(true)

    const currFormData = new FormData(event.target);
    setFD(currFormData);
  };

  const handleDeleteIngredient = (e, cellValues) => {
    e.stopPropagation();

    setIsDeleteIngredClicked(true);
    setCurrentCocktailId(cellValues.row.id);
    setIngredientToDelete(
      e.target.previousSibling?.innerHTML && e.target.previousSibling?.innerHTML
    );
  };

  const handleAddIngredient = (cellValues) => {
    
    handleClickOpenAddIngredients()
    setCurrentCocktailId(cellValues.row.id);
  };

  const handleEditName = (cellValues) => {
    setIsEditNameBtnClicked(true)
    handleClickOpenEditCocktail();
    setCurrentCocktailId(cellValues.row.id)
  }

  const handleEditImage = (cellValues) => {
    setIsEditImageBtnClicked(true)
    handleClickOpenEditCocktail();
    setCurrentCocktailId(cellValues.row.id)
  }

  // -----------------   Set Columns and Rows of Table ------------------- // useMemo(() =>
 const columns =[
  {
    field: 'id',
    align: 'center',
    headerAlign: 'center',
    headerName: 'ID',
    width: 50,
  },
  {
    field: 'name',
    align: 'center',
    headerAlign: 'center',
    headerName: 'Name',
    editable: true,
    width: 150,
    renderCell: (params) => {
      return (
        <Box onClick={() => handleEditName(params)}>
          {params.value}
          <EditIcon />
        </Box>
      );
    },
  },
  {
    field: 'imageUrl',
    type: 'image',
    align: 'center',
    headerAlign: 'center',
    headerName: 'Image',
    width: 100,
    renderCell: (params) => {
      return (
        <Box onClick={() => handleEditImage(params)}>
            <img
            style={{ width: '45px', height: '45px' }}
            src={params.value}
            alt=""
          />
          <EditIcon />
        </Box>
      );
    },
  },
  {
    field: 'favouriteCount',
    align: 'center',
    headerAlign: 'center',
    headerName: 'Favs',
    width: 60,
  },
  {
    field: 'ingredients',
    align: 'center',
    headerAlign: 'center',
    headerName: 'Ingredients',
    minWidth: 470,
    renderCell: (params) => {
      return params.value.map((i) => (
        <div key={i.id}>
          
          <Stack  direction="row" spacing={1}>
            <Chip sx={{color: "white", "& .MuiChip-deleteIcon": {color: "rgb(240,55,55)"}, "& .MuiChip-deleteIcon:hover": {color: "rgb(211,37,37)"}}} label={i.name} variant="filled"  onDelete={(e) => {
              handleDeleteIngredient(e, params);
            }} />  
          </Stack>

        </div>
      ));
    },
  },
  {
    field: 'add',
    align: 'center',
    headerAlign: 'center',
    headerName: 'Add Ingredient',
    type: 'button',
    width: 100,
    renderCell: (cellValues) => {
      return (
        <AddCircleIcon
          onClick={() => {
            handleAddIngredient(cellValues);
          }}
          color="success"
        />
      );
    },
  },
];
 
  const rows = cocktails.map((c) => {
    return {
      id: c.id,
      name: c.name,
      imageUrl: API_URL + c.imageUrl,
      favouriteCount: c.favouriteCount,
      ingredients: c.ingredients,
    };
  });
  // ------------------ End of Setting Table Columns and Rows  -----------------//

  // --- Get All Cocktails ---  //
  useEffect(() => {
    getAllCocktails(setCocktails);
  }, [isSubmitEditNameBntClicked, isSubmitEditImageBntClicked, isSubmitCocktailClicked, isDeleteIngredClicked, isAddIngredBtnClicked]);

  useEffect(() => {
    // --- Delete ingredients --- //
    if (isDeleteIngredClicked) {
      deleteIngredientFromCocktail(currentCocktailId, ingredientToDelete);
      setIsDeleteIngredClicked(false);
    }

    //--- Change Name of Cocktail --- //
    if (isSubmitEditNameBntClicked) {
      editCocktailName(currentCocktailId, cocktailName);
      setIsSubmitEditNameBntClicked(false);
      setCocktailName('')
    }

    // --- Change Image of Cocktail --- //
    if (isSubmitEditImageBntClicked) {
      editCocktailImage(currentCocktailId, fd)
      setIsSubmitEditImageBntClicked(false);
    }

    // --- Add New Cocktail --- //
    if (isSubmitCocktailClicked) {   
      addNewCocktail(formData);
      setIsSubmitCocktailClicked(false);
      setCocktailName('')
    }

  }, [
    isAddIngredBtnClicked,
    isDeleteIngredClicked,
    isSubmitEditNameBntClicked,
    isEditImageBntClicked,
    isSubmitCocktailClicked,
    ingredientToDelete,
    formData,
    currentCocktailId,
    cocktailName,
    fd,
    isSubmitEditImageBntClicked
  ]);

  // --------------------------------- Rendering  ------------------------------------------------//

  return (
    <div>
      <div className="add-cocktail-btn-box">
        <Button className="add-cocktail-btn" onClick={handleClickOpenAddCocktail}> Add New Cocktail</Button> 
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Add Cocktail Form</DialogTitle>
          <DialogContent>
            <Box component="form" onSubmit={handleSubmit}>
              <TextField
                autoFocus
                required={!cocktailNameError}
                margin="dense"
                id="name"
                label='Cocktail Name'
                type="text"
                fullWidth
                name="name"
                value={cocktailName}
                onChange={(e) => setCocktailName(e.target.value)}
                error={cocktailNameError}
                helperText={cocktailNameError && `Cocktail Name must be between ${COCKTAIL_NAME_MIN_LENGTH} and ${COCKTAIL_NAME_MAX_LENGTH} symbols`}
              />
              <TextField
                required
                autoFocus
                margin="dense"
                id="ingredients"
                label="Ingredients"
                type="text"
                fullWidth
                name="ingredients"
                helperText="Please separate every ingredient by coma"
              />
              Add Cocktail Image
                <TextField
                autoFocus
                margin="dense"
                id="cocktail-image"
                type="file"
                name="image"
                fullWidth
                hidden
                required
              />
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button type="submit">Submit</Button>
              </DialogActions>
            </Box>
          </DialogContent>
        </Dialog>

        <Dialog open={openEdit} onClose={handleClose}>
          <DialogTitle>{isEditNameBntClicked ? 'Edit Cocktail Name' : 'Edit Cocktail Image'}</DialogTitle>
          <DialogContent>
            <Box> 
              {isEditNameBntClicked &&
                <Box component="form" onSubmit={handleSubmitName}>
                  <TextField
                    autoFocus
                    required={!cocktailNameError}
                    margin="dense"
                    id="name"
                    label='Change Cocktail Name'
                    type="text"
                    fullWidth
                    name="name"
                    value={cocktailName}
                    onChange={(e) => setCocktailName(e.target.value)}
                    error={cocktailNameError}
                    helperText={cocktailNameError && `Cocktail Name must be between ${COCKTAIL_NAME_MIN_LENGTH} and ${COCKTAIL_NAME_MAX_LENGTH} symbols`}
                  /> 
                  <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button type="submit">Submit</Button>
                  </DialogActions>
                </Box>
              }
             {isEditImageBntClicked && 
                <Box component="form" onSubmit={handleSubmitImage}>
                  <TextField
                    autoFocus
                    margin="dense"
                    id="cocktail-image"
                    type="file"
                    name="image"
                    fullWidth
                    hidden
                    required
                  />
                  <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button type="submit">Submit</Button>
                  </DialogActions>
                </Box>
              }
            </Box>
          </DialogContent>
        </Dialog>
        <Dialog open={openAddIngredients} onClose={handleClose}>
          <ListIngredients  cocktailId={currentCocktailId} hasNewIngredients={() => setIsAddIngredBtnClicked(true)} onclose={handleClose} />
        </Dialog>
      </div>
      <div className="ingredients-table">
        <DataGrid
          sx={{ color: "white" }}
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[10]}
          editMode="cell"
          autoWidth={true}
        />
      </div>
    </div>
  );
};

export default CocktailsTable;
