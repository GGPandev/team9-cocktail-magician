import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import { AuthContext } from '../../Providers/AuthContext';
import Button from '@mui/material/Button';
import './Bars.scss';
import ListBarBox from '../../Components/ListBarBox/ListBarBox';

const Bars = () => {
  // States:
  const [allAvailableIngredients, setAllAvailableIngredients] = useState([]);
  const [allBarsArr, setAllBarsArr] = useState([]);
  const [allCocktailsArr, setAllCocktailsArr] = useState([]);
  const [foundBarsByCocktailIngredients, setFoundBarsByCocktailIngredients] =
    useState([]);
  const [isSearchingByBarName, setIsSearchingByBarName] = useState(true);
  const [searchBy, setSearchBy] = useState('byBarName');
  const [foundCocktails, setFoundCocktails] = useState([]);
  // Contexts:
  const { auth } = useContext(AuthContext);

  // Fetching All Bars, and All Cocktails:
  useEffect(() => {
    axios.get(API_URL + '/bars').then((res) => setAllBarsArr(res.data));
    axios
      .get(API_URL + '/cocktails')
      .then((res) => setAllCocktailsArr(res.data));
  }, []);

  // Making a list of all used ingredients in the available cocktails:
  useEffect(() => {
    if (allCocktailsArr.length) {
      let allIngredients = [];
      let mappedCocktailsArr = allCocktailsArr.map((cocktail) =>
        cocktail.ingredients.map((ingr) => ingr.name)
      );

      mappedCocktailsArr.forEach((arr) => {
        arr.map((ingredient) => allIngredients.push(ingredient));
      });

      const result = Array.from(new Set(allIngredients)).sort((a, b) =>
        a.localeCompare(b)
      );
      // console.log(result);
      setAllAvailableIngredients(result);
    }
  }, [allCocktailsArr]);

  // Searching Bars by bar name:
  const searchByBarName = (e) => {
    setIsSearchingByBarName(true);
    const searchedBar = e.target.parentElement.getElementsByTagName('input')[0];
    axios
      .get(API_URL + `/bars?name=${searchedBar.value}`)
      .then((res) => setAllBarsArr(res.data))
      .catch((err) => console.log(err));
    // searchedBar.value = '';
  };

  // Searching Bars by cocktail ingredients:
  const searchByCocktailIngredients = (e) => {
    setIsSearchingByBarName(false);

    axios.get(API_URL + `/bars`).then((res) => setAllBarsArr(res.data));

    const inputSearchedIngredients =
      e.target.parentElement.getElementsByTagName('input')[0];

    const searchedIngredients = inputSearchedIngredients.value
      .split(',')
      .map((ingredient) => ingredient.trim().toLowerCase())
      .filter((ingredient) => ingredient);

    const searchedIngredientsUnique = Array.from(new Set(searchedIngredients));
    const correctIngredients = searchedIngredientsUnique.filter(
      (currIngredient) => allAvailableIngredients.includes(currIngredient)
    );

    const searchedIngredientsQuery = correctIngredients
      .map((ingredient) => `values=${ingredient}`)
      .join('&');

    axios
      .get(
        API_URL +
          `/bars/cocktail-ingredients` +
          (searchedIngredientsQuery ? `?${searchedIngredientsQuery}` : '')
      )
      .then((res) => setFoundCocktails(res.data))
      .catch((err) => console.log(err));

    // inputSearchedIngredients.value = '';
  };

  // Finding Bars by cocktail ingredients:
  useEffect(() => {
    if (foundCocktails.length) {
      const data = foundCocktails.slice();

      const numOfIngredients = data.length;
      const result = {};

      data.forEach((ingredient) => {
        ingredient.bars.forEach((currBarCocktail) => {
          if (!result[currBarCocktail.barId]) {
            result[currBarCocktail.barId] = {};
          }

          if (
            !result[currBarCocktail.barId][
              currBarCocktail.cocktailId + '-' + currBarCocktail.cocktailName
            ]
          ) {
            result[currBarCocktail.barId][
              currBarCocktail.cocktailId + '-' + currBarCocktail.cocktailName
            ] = [];
          }

          result[currBarCocktail.barId][
            currBarCocktail.cocktailId + '-' + currBarCocktail.cocktailName
          ].push(ingredient.ingredientName);
        });
      });

      for (let bar in result) {
        for (let cocktail in result[bar]) {
          if (result[bar][cocktail].length < numOfIngredients) {
            delete result[bar][cocktail];
          }
        }

        if (Object.values(result[bar]).length === 0) {
          delete result[bar];
        }
      }

      const transformedResult = [];

      for (let bar in result) {
        const currBarObj = {};
        currBarObj[`barId`] = Number(bar);

        currBarObj[`cocktailsWithIngredients`] = [];

        for (let cocktail in result[bar]) {
          currBarObj[`cocktailsWithIngredients`].push(
            Number(cocktail.split('-')[0])
          );
        }

        transformedResult.push(currBarObj);
      }
      setFoundBarsByCocktailIngredients(transformedResult);
    }
  }, [foundCocktails]);

  return (
    <div>
      <div className="bars-search-container">
        <div className="title-radioButtons-flexContainer">
          <h3>Search Bar by: </h3>
          <form
            onChange={(e) => setSearchBy(e.target.value)}
            className="searchBy-form"
          >
            <fieldset>
              <input
                type="radio"
                name="searchBy"
                id="radioSearchByBarName"
                value="byBarName"
                defaultChecked
              />
              <label htmlFor="radioSearchByBarName">Name</label>

              <input
                type="radio"
                name="searchBy"
                id="radioSearchByCocktail"
                value="byCocktail"
              />
              <label htmlFor="radioSearchByCocktail">
                Cocktail Ingredients
              </label>
            </fieldset>
          </form>
        </div>

        {searchBy === 'byBarName' ? (
          <div className="searchField-button">
            <input
              className="searchField"
              type="search"
              name="inputSearchByBarName"
              id="inputSearchByBarName"
            />
            <Button
              className="searchButton"
              variant="contained"
              onClick={searchByBarName}
            >
              Search
            </Button>
          </div>
        ) : (
          <div className="searchField-button">
            <input
              className="searchField"
              type="search"
              name="inputSearchByCocktailIngredients"
              id="inputSearchByCocktailIngredients"
              placeholder="rum, lime, ice"
            />
            <Button
              className="searchButton"
              variant="contained"
              onClick={searchByCocktailIngredients}
            >
              Search
            </Button>
          </div>
        )}
      </div>

      <div className="bars-searchList-container">
        <h1>
          {isSearchingByBarName
            ? allBarsArr.length
            : foundBarsByCocktailIngredients.length}{' '}
          Bar/s Found:
        </h1>

        {/* Rendering bars based on NAME SEARCH */}
        {isSearchingByBarName
          ? allBarsArr
              .sort((a, b) => a.name.localeCompare(b.name))
              .map((bar) => <ListBarBox key={bar.id} bar={bar} />)
          : /* Rendering bars based on COCKTAIL INGREDIENTS SEARCH */
            foundBarsByCocktailIngredients.map((bar) => (
              <div key={bar.barId}>
                {allBarsArr
                  .filter((barObj) => barObj.id === bar.barId)
                  .map((filteredBar) => (
                    <ListBarBox
                      key={bar.id}
                      bar={filteredBar}
                      barObj={bar}
                      allCocktailsArr={allCocktailsArr}
                    />
                  ))}

                <br />
              </div>
            ))}
      </div>
    </div>
  );
};

export default Bars;
