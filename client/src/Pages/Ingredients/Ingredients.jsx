import React, { useEffect, useState } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Button, TextField } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import './Ingredients.scss';
import { addIngredient, deleteIngredient, getIngredients } from '../../Requests/requests';

const Ingredients = () => {
  const [ingredients, setIngredients] = useState([]);
  const [isAddBtnClicked, setIsAddBtnClicked] = useState(false);
  const [isDeleteBtnClicked, setDeleteIsBtnClicked] = useState(false);
  const [ingredientID, setIngredientID] = useState(0);
  const [ingredientName, setIngredientName] = useState('');

  const columns = [
    {
      field: 'id',
      align: 'center',
      headerAlign: 'center',
      headerName: 'ID',
      width: 150,
    },
    {
      field: 'name',
      align: 'center',
      headerAlign: 'center',
      headerName: 'Name',
      width: 280,
    },
    {
      field: 'cocktailsCount',
      align: 'center',
      headerAlign: 'center',
      headerName: 'Cocktails Count',
      width: 280,
    },
    {
      field: 'delete',
      align: 'center',
      headerAlign: 'center',
      headerName: 'Delete',
      type: 'button',
      width: 200,
      renderCell: () => {
        return (
          <Button className="delete-ingredient-btn"
            variant="contained"
            startIcon={<DeleteIcon />}
            onClick={(e) => handleDelete(e)}
          >
            Delete
          </Button>
        );
      },
    },
  ];

  const handleChange = (event) => {
    setIngredientName(event.target.value);
  };

  const handleDelete = (e) => {
    setDeleteIsBtnClicked(true);
    setIngredientID(e.target.parentElement.parentElement.firstChild.innerText);
  };

  useEffect(() => {
    getIngredients(setIngredients);
  }, [isAddBtnClicked, isDeleteBtnClicked]);

  useEffect(() => {
    if (isDeleteBtnClicked && ingredientID > 0) {
      deleteIngredient(ingredientID);
      setDeleteIsBtnClicked(false);
    }
  }, [ingredientID, isDeleteBtnClicked]);

  useEffect(() => {
    if (isAddBtnClicked) {
      addIngredient(ingredientName);
      setIsAddBtnClicked(false);
      setIngredientName('');
    }
  }, [ingredientName, isAddBtnClicked]);

  return (
    <div className="table-box">
      <div
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          display: 'inline-block',
          padding: '5px',
          width: '99%',
        }}
      >
        <TextField
          sx={{width: '80%', marginRight: '10px', input: { color: 'white' }, label: { color: 'white' } }}
          id="outlined-name"
          label="Ingredient Name"
          value={ingredientName}
          onChange={handleChange}
          variant="filled"
        />

        <Button
          className="add-ingredients"
          variant="contained"
          size="large"
          onClick={() => setIsAddBtnClicked(true)}
        >
          Add Ingredient
        </Button>
      </div>
      <div
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          height: 635,
          width: '100%',
        }}
        className="ingredients-table"
      >
        <DataGrid
          rows={ingredients}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
        />
      </div>
    </div>
  );
};

export default Ingredients;
