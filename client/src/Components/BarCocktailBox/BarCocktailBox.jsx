import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import { AuthContext, getToken } from '../../Providers/AuthContext';
import CocktailID from '../../Providers/CocktailID';
import SimpleButton from '../SimpleButton/SimpleButton';
import './BarCocktailBox.scss';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddCircleIcon from '@mui/icons-material/AddCircle';

const BarCocktailBox = ({
  id,
  barId,
  imageUrl,
  name,
  ingredients,
  onclick,
  buttonType,
}) => {
  // Contexts:
  const { auth } = useContext(AuthContext);
  const { setCocktailID } = useContext(CocktailID);

  const authAxios = axios.create({
    baseURL: API_URL,
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });

  // Cocktail Requests:
  const cocktailRequest = (id, barId) => {
    if (buttonType === 'delete') {
      authAxios.delete(`/bars/${barId}/cocktails/${id}`);
    } else if (buttonType === 'add') {
      authAxios.put(`/bars/${barId}/cocktails/${id}`);
    }
    onclick();
  };

  const cocktailDetailsRequest = (cocktailId) => {
    sessionStorage.setItem('cocktailId', cocktailId);
  };

  return (
    <div className="barCocktailBox-cocktailDetailsBtn">
      <div className="barCocktailBox">
        <div className="barCocktailBox-image">
          <img src={API_URL + imageUrl} alt={name} />
        </div>
        <div className="barCocktailBox-info">
          <h4>{name}</h4>
          <p>{ingredients}</p>
        </div>
        {auth.user && auth.user.role === 'magician' && (
          <button
            className="cocktails-details-deleteBtn"
            onClick={() => cocktailRequest(id, barId)}
          >
            {buttonType === 'delete' ? (
              <DeleteForeverIcon />
            ) : (
              <AddCircleIcon />
            )}
          </button>
        )}
      </div>
      <div className="cocktailDetailsBtn">
        <Link to="/cocktails-details">
          <SimpleButton
            onclick={() => cocktailDetailsRequest(id)}
            buttonLabel="Cocktail Details"
          />
        </Link>
      </div>
    </div>
  );
};

export default BarCocktailBox;
