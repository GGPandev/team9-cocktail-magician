import { Rating } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import './CocktailBarBox.scss'
import { Button } from '@mui/material';


const CocktailBarBox = ({id, url, name, address, phone, rating}) => {
  return (
    <div key={id} className="bar-info">
      <div className="bar-info-img">
        <img src={url && API_URL + url} alt={name} />
      </div>
      <div className="bar-info-details">
        <h3 className="bars-list">{name}</h3>
        <p className="bars-list">{address}</p>
        <p className="bars-list">{phone}</p>
      </div>
      <Rating
        name="half-rating"
        value={rating ? rating : 0}
        precision={0.1}
        readOnly
      />
      <Link to="/bar-details"><Button variant="contained" onClick={() => sessionStorage.setItem("barId", id)}>Bar Details</Button></Link>
    </div>
  );
};

export default CocktailBarBox;
