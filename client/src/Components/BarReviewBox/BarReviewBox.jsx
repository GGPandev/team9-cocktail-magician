import React, { useContext } from 'react';
import { AuthContext } from '../../Providers/AuthContext';
import SimpleButton from '../SimpleButton/SimpleButton';
import { Link } from 'react-router-dom';
import { Rating } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import './BarReviewBox.scss';
// import UserContext from '../../Providers/UserContext';

const BarReviewBox = ({
  reviewId,
  reviewText,
  reviewRating,
  authorId,
  authorUsername,
  authorDisplayName,
}) => {
  // Contexts:
  const { auth } = useContext(AuthContext);
  // const { setUserId } = useContext(UserContext);

  // const userDetailsRequest = (userId) => {
  //   setUserId(userId);
  //   console.log(userId);
  // };

  return (
    <div className="barReviewBox">
      <div className="barReview-header-rating">
        <div className="barReview-header">
          <div className="barReview-profileIcon">
            <AccountCircleIcon sx={{ fontSize: 60 }} />
          </div>
          <div className="barReview-userName-userButton">
            <h3>{authorDisplayName ? authorDisplayName : authorUsername}</h3>
            {auth.user &&
              (auth.user.role === 'magician' ||
                auth.user.role === 'crawler') && (
                <Link to="/user-details">
                  <SimpleButton
                    onclick={() => sessionStorage.setItem('userId', authorId)}
                    buttonLabel="View Author's Profile"
                  />
                </Link>
              )}
          </div>
        </div>

        <div className="barReview-rating-container">
          <div className="barReview-stars">
            <Rating
              name="half-rating"
              value={reviewRating ? reviewRating : 0}
              precision={0.1}
              readOnly
            />
          </div>
          <div className="barReview-text">
            <p>/ Rating: {reviewRating} /</p>
          </div>
        </div>
      </div>

      <p>{reviewText}</p>
    </div>
  );
};

export default BarReviewBox;
