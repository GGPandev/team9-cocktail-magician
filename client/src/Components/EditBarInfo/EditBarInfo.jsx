import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@mui/material';
import axios from 'axios';
import React, { useState } from 'react';
import { API_URL } from '../../Constants/constants';
import { getToken } from '../../Providers/AuthContext';
import './EditBarInfo.scss';

const EditBarInfo = ({ barId, name, phone, address, onclick }) => {
  const initialValues = {
    name: name,
    phone: phone,
    address: address,
  };

  const [data, setData] = useState(initialValues);
  const [open, setOpen] = useState(false);

  // Handlers:

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (e) => {
    // console.log(e.target);
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
    console.log(data);
  };

  const submitFormHandler = (event) => {
    event.preventDefault();

    console.log('Update button is pressed!!!');

    axios({
      url: `${API_URL}/bars/${barId}`,
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      data: {
        name: data.name,
        phone: data.phone,
        address: data.address,
      },
    })
      .then((res) => {
        console.log(res);
        handleClose();
        onclick();
      })
      .catch((err) => console.log(err.message));
  };

  return (
    <div className="editBarInfo-container">
      <button className="editInfoBtn" onClick={handleClickOpen}>
        Edit Info
      </button>
      <Dialog open={open} onClose={handleClose}>
        <form className="addBar-form" onSubmit={submitFormHandler} action="">
          <DialogTitle>Update Bar Info:</DialogTitle>
          {/* <h3>How would you rate this bar?</h3> */}

          <DialogContent>
            {/* <textarea
              onChange={handleInputChange}
              autoComplete="off"
              required
              name="text"
              id="reviewText"
              cols="60"
              rows="20"
              minLength="25"
              maxLength="1000"
              value={data.text}
            ></textarea> */}
            {/* <p>(Your review should be 25-1000 characters long)</p> */}
            {/* <DialogContentText>
              To subscribe to this website, please enter your email address
              here. We will send updates occasionally.
            </DialogContentText> */}
            <TextField
              id="name"
              name="name"
              onChange={handleInputChange}
              value={data.name}
              label="Name:"
              type="text"
              autoComplete="off"
              fullWidth
              variant="standard"
              margin="dense"
            />
            <TextField
              id="phone"
              name="phone"
              onChange={handleInputChange}
              value={data.phone}
              label="Phone:"
              type="text"
              autoComplete="off"
              fullWidth
              variant="standard"
              margin="dense"
            />
            <TextField
              id="address"
              name="address"
              onChange={handleInputChange}
              value={data.address}
              label="Address:"
              type="text"
              autoComplete="off"
              fullWidth
              variant="standard"
              margin="dense"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button type="submit">Update Info</Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

export default EditBarInfo;
