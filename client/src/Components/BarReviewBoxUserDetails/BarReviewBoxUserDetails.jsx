import { Rating } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';
import './BarReviewBoxUserDetails.scss';

const BarReviewBoxUserDetails = ({
  barId,
  barName,
  barReview,
  userRating,
  averageRating,
}) => {
  return (
    <div className="barReviewBoxUserDetails">
      <div className="barReviewBoxUserDetails-left">
        <Link
          to="/bar-details"
          onClick={() => sessionStorage.setItem('barId', barId)}
        >
          <h2>{barName}</h2>
        </Link>
        <div className="barReview-barGrade">
          <div className="barReview-stars">
            <Rating
              name="half-rating"
              value={averageRating ? averageRating : 0}
              precision={0.1}
              readOnly
            />
          </div>
          <div className="barReview-number">
            <h3>{averageRating}</h3>
          </div>
        </div>
      </div>

      <div className="barReviewBoxUserDetails-right">
        <div className="barReview-userGrade">
          <div className="barReview-stars">
            <Rating
              name="half-rating"
              value={userRating ? userRating : 0}
              precision={0.1}
              readOnly
            />
          </div>
          <div className="barReview-number">
            <h3>{userRating}</h3>
          </div>
        </div>

        <p>{barReview}</p>
      </div>
    </div>
  );
};

export default BarReviewBoxUserDetails;
