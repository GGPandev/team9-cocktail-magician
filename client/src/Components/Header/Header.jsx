import { Box } from '@mui/system';
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import { AuthContext } from '../../Providers/AuthContext';
import { logout } from '../../Services/logout';
import { authorizationHeader } from '../../Utils/axiosHeaders';
import './Header.scss';

const Header = () => {
  const { auth, setAuth } = useContext(AuthContext);
  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    axios
      .get(API_URL + `/users/me`, { headers: authorizationHeader })
      .then((res) => setUserInfo(res.data));
  }, []);

  return (
    <div className="header">
      <NavLink to="/bars">Bars</NavLink>
      <NavLink to="/cocktails">Cocktails</NavLink>
      {auth.isLoggedIn && auth.user.role === 'magician' && (
        <>
          <NavLink to="/add-bar">Add Bar</NavLink>
          <NavLink to="/admin/ingredients">Ingredients List</NavLink>
          <NavLink to="/admin/cocktails">Cocktails List</NavLink>
        </>
      )}
      {auth.isLoggedIn ? (
        <NavLink to="/login" onClick={() => logout(setAuth)}>
          Logout
        </NavLink>
      ) : (
        <NavLink to="/login">Login</NavLink>
      )}

      {auth.isLoggedIn ? (
        <Box style={{ display: 'inline-block', color: 'white' }}>
          Welcome,{' '}
          <Link
            className="profile-link"
            to="/user-profile-details"
            onClick={() => sessionStorage.setItem('userId', auth.user.sub)}
          >
            {userInfo.displayName ? userInfo.displayName : userInfo.username}
          </Link>
        </Box>
      ) : (
        <div style={{ display: 'inline-block', color: 'white' }}>
          Welcome, anonymous
        </div>
      )}
    </div>
  );
};

export default Header;
