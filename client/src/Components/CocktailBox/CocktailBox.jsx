import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import './CocktailBox.scss';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarRoundedIcon from '@mui/icons-material/StarRounded';
import { AuthContext } from '../../Providers/AuthContext';

const CocktailBox = ({
  id,
  imageUrl,
  name,
  ingredients,
  favouriteCount,
  onclick,
  onclickStarToLike,
  onclickStarToUnlike,
  favoriteTo,
  button
}) => {

  const { auth } = useContext(AuthContext);

  return (
    <>
      <div className="cocktail-container" key={id}>
        { favoriteTo?.length ? (
          auth.isLoggedIn && <StarRoundedIcon
            onClick={() => onclickStarToUnlike(id)}
            className="star"
          />
        ) : (
          auth.isLoggedIn && <StarBorderIcon
            className="star"
            onClick={() => onclickStarToLike(id)}
          />
        )}

        <div className="cocktail-box-img">
          <img src={imageUrl && API_URL + imageUrl} alt={name} />
        </div>
        <div>
          <h3>{name}</h3>
          <p>{ingredients?.map((i) => i.name).join(', ')}</p>
          <p>Favorite to: {favouriteCount} cocktail Lovers</p>
          {button ? <Link to="/cocktails-details">
            <button data-id={id} onClick={onclick}>
              Cocktail Details
            </button>
          </Link> 
          : null}
        </div>
      </div>
    </>
  );
};

export default CocktailBox;
