import React, { useEffect, useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import { Button, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { addIngredientsToCocktail, getIngredients } from '../../Requests/requests';

const ListIngredients = ({ cocktailId, hasNewIngredients, onclose }) => {
  const [checked, setChecked] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [isAddBtnClicked, setIsAddBtnClicked] = useState(false);
  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };
  useEffect(() => {
    getIngredients(setIngredients);
  }, [isAddBtnClicked]);

  useEffect(() => {
    if (isAddBtnClicked) {
      addIngredientsToCocktail(cocktailId, checked)
      setIsAddBtnClicked(false);
    }
  }, [isAddBtnClicked]);

  return (
    <Box >
      <TextField label="Search Ingredient" variant="filled" name=""  />
      <List
        sx={{
          width: '100%',
          maxWidth: 500,
          bgcolor: 'background.paper',
        }}
      >
        <Button style={{ position: "sticky", top: "2px", width: "100%"}}  variant="contained" onClick={() => {setIsAddBtnClicked(true); hasNewIngredients(); onclose()}}>Add Ingredients</Button>
        {ingredients
          .sort((a, b) => a.name.localeCompare(b.name))
          .map((ingredient) => {
            const labelId = `checkbox-list-label-${ingredient.name}`;

            return (
              <ListItem  key={ingredient.id}>
                <ListItemButton
                  role={undefined}
                  onClick={handleToggle(ingredient)}
                  dense
                >
                  <ListItemIcon>
                    <Checkbox
                      edge="start"
                      checked={checked.indexOf(ingredient) !== -1}
                      tabIndex={-1}
                      disableRipple
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </ListItemIcon>
                  <ListItemText id={labelId} primary={ingredient.name} />
                </ListItemButton>
              </ListItem>
            );
          })}
      </List>
    </Box>
  );
};

export default ListIngredients;
