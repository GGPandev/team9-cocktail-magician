import React from 'react';
import logo from '../../images/CMM.png';
import './Footer.scss';

const Footer = () => {
  return (
    <div className="footer-container">
      <h2>Cocktail Magician Manager</h2>
      <p>Telerik Academy ©2021</p>
      <p>Alpha 32 JS - Team 9</p>
    </div>
  );
};

export default Footer;
