import React from 'react';
import { API_URL } from '../../Constants/constants';
import { Link } from 'react-router-dom';

import './CocktailBoxSmall.scss';

const CocktailBoxSmall = ({ id, name, imageUrl, onclick }) => {
  return (
    <Link
      to="/cocktails-details"
      onClick={() => sessionStorage.setItem('cocktailId', id)}
    >
      <div className="cocktailBoxSmall-container">
        <div className="cocktailBoxSmall-image">
          <img src={API_URL + imageUrl} alt={name}></img>
        </div>
        <div className="cocktailBoxSmall-name">
          <p>{name}</p>
        </div>
      </div>
    </Link>
  );
};

export default CocktailBoxSmall;
