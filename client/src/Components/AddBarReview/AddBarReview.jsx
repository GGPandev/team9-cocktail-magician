import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Rating } from '@mui/material';
import './AddBarReview.scss';
import axios from 'axios';
import { API_URL } from '../../Constants/constants';
import { getToken } from '../../Providers/AuthContext';

const initialValues = {
  text: '',
  rating: 0,
};

const AddBarReview = ({ barId, isBarReviewedByUser, onclick }) => {
  const [data, setData] = useState(initialValues);
  const [open, setOpen] = useState(false);

  // Handlers:
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name === 'text') {
      setData({ ...data, [name]: value });
    } else {
      setData({ ...data, [name]: Number(value) });
    }
    console.log(data);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const submitFormHandler = (event) => {
    event.preventDefault();

    console.log('Submit button is pressed!!!');

    axios({
      url: `${API_URL}/bars/${barId}/reviews`,
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      data: {
        text: data.text,
        rating: Number(data.rating),
      },
    })
      .then((res) => {
        console.log(res);
        handleClose();
        onclick();
      })
      .catch((err) => console.log(err.message));
  };

  return (
    <div className="addBarReview-container">
      {isBarReviewedByUser === false ? (
        <>
          <button className="addReviewBtn" onClick={handleClickOpen}>
            Add Review
          </button>
          <Dialog open={open} onClose={handleClose}>
            <form
              className="addBar-form"
              onSubmit={submitFormHandler}
              action=""
            >
              <DialogTitle>Add Bar Review</DialogTitle>
              <h3>How would you rate this bar?</h3>
              <Rating
                onChange={handleInputChange}
                required
                name="rating"
                precision={1}
                value={data.rating}
              />
              <DialogContent>
                <textarea
                  onChange={handleInputChange}
                  autoComplete="off"
                  required
                  name="text"
                  id="reviewText"
                  cols="60"
                  rows="20"
                  minLength="25"
                  maxLength="1000"
                  value={data.text}
                ></textarea>
                <p>(Your review should be 25-1000 characters long)</p>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button type="submit">Add Review</Button>
              </DialogActions>
            </form>
          </Dialog>
        </>
      ) : (
        <p className="alreadyReviewed">Already Reviewed</p>
      )}
    </div>
  );
};
export default AddBarReview;
