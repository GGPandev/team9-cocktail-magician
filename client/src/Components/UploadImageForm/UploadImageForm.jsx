import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import BackupIcon from '@mui/icons-material/Backup';
import axios from 'axios';
import { API_URL } from '../../Constants/constants';
import { getToken } from '../../Providers/AuthContext';
import './UploadImageForm.scss';

const UploadImageForm = ({ barId, onclick }) => {
  const [file, setFile] = useState(null);
  const [open, setOpen] = useState(false);

  // Handlers:
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFile = (e) => {
    // console.log(e.target.files, '$$$$');
    // console.log(e.target.files[0], '$$$$');
    setFile(e.target.files[0]);
  };

  const submitFormHandler = (event) => {
    event.preventDefault();

    const formData = new FormData();

    formData.append('image', file);

    console.log([...formData.entries()]);

    axios({
      url: `${API_URL}/bars/${barId}/images`,
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      data: formData,
    })
      .then((res) => {
        console.log(res);
        handleClose();
        onclick();
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="uploadImage-container">
      <button className="uploadImageBtn" onClick={handleClickOpen}>
        <AddPhotoAlternateIcon sx={{ fontSize: 38 }} />
      </button>
      <Dialog open={open} onClose={handleClose}>
        <form
          className="uploadImage-form"
          onSubmit={submitFormHandler}
          action=""
        >
          <DialogTitle>Upload Image To Gallery</DialogTitle>

          <TextField
            id="cover-image"
            type="file"
            multiple="multiple"
            required
            name="images"
            variant="outlined"
            onChange={handleFile}
            fullWidth
            margin="dense"
          />
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button type="submit" variant="contained" endIcon={<BackupIcon />}>
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};
export default UploadImageForm;
