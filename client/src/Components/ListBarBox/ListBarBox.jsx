import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Rating } from '@mui/material';
import CallIcon from '@mui/icons-material/Call';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import { API_URL } from '../../Constants/constants';
import './ListBarBox.scss';

const ListBarBox = ({ bar, barObj, allCocktailsArr }) => {
  return (
    <div className="singleBar-container">
      <div className="barInfo-container">
        <div className="barInfo-barImage">
          <img src={bar.url && API_URL + bar.url} alt={bar.name} />
        </div>

        <div className="barInfo-text">
          <h2>{bar.name}</h2>

          <h4>
            <div className="barInfo-textIcon">
              <LocationOnIcon />
            </div>
            {bar.address}
          </h4>

          <h4>
            <CallIcon />
            {bar.phone}
          </h4>
        </div>

        <div className="barInfo-rating-button">
          <div className="barInfo-rating-container">
            <div className="barInfo-stars">
              <Rating
                name="half-rating"
                value={bar.rating ? bar.rating : 0}
                precision={0.1}
                readOnly
              />
            </div>
            <div className="barInfo-text">
              <p>/ Rating: {bar.rating ? bar.rating : 'Not Available'} /</p>
            </div>
          </div>

          <div className="barInfo-button-container">
            <Link
              onClick={() => sessionStorage.setItem('barId', bar.id)}
              to="/bar-details"
            >
              <Button className="detailsButton" variant="contained">
                Details
              </Button>
            </Link>
          </div>
        </div>
      </div>

      {/* Check if we are searching for Bars by cocktail ingredients, and if yes -> Render the section below */}
      {barObj && barObj.barId ? (
        <div className="barCocktails-container">
          <h3 className="barCocktails-title">
            Cocktails containing the searched ingredients:
          </h3>
          <ul className="barCocktails-list">
            {barObj.cocktailsWithIngredients.map((cocktail) => (
              <li key={cocktail}>
                {allCocktailsArr
                  .filter((cocktailObj) => cocktailObj.id === cocktail)
                  .map((filteredCocktail) => (
                    <div key={'Cocktail' + cocktail}>
                      <h4>
                        {filteredCocktail.name}{' '}
                        <span className="barCocktails-ingredients-p">
                          /
                          {filteredCocktail.ingredients
                            .map((ingr) => ingr.name)
                            .join(', ')}
                          /{' '}
                        </span>
                      </h4>
                    </div>
                  ))}
              </li>
            ))}
          </ul>
        </div>
      ) : null}
    </div>
  );
};

export default ListBarBox;
