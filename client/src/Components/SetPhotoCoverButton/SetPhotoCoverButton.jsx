import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { API_URL } from '../../Constants/constants';
import { AuthContext, getToken } from '../../Providers/AuthContext';
import ImageIcon from '@mui/icons-material/Image';
import './SetPhotoCoverButton.scss';
import axios from 'axios';

const SetPhotoCoverButton = ({ barId, imageId, onclick }) => {
  const submitRequestHandler = () => {
    console.log('Button Clicked!');
    axios({
      url: `${API_URL}/bars/${barId}/images/${imageId}/cover`,
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((res) => {
        console.log(res);
        onclick();
      })
      .catch((err) => console.log(err.message));
  };

  return (
    <div onClick={submitRequestHandler} className="setPhotoCoverButton">
      <ImageIcon />
    </div>
  );
};

export default SetPhotoCoverButton;
