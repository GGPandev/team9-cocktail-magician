import React from 'react';
import './SimpleButton.scss';

const SimpleButton = ({ buttonLabel, onclick }) => {
  return (
    <button onClick={onclick} className="simpleButton">
      {buttonLabel}
    </button>
  );
};

export default SimpleButton;
