import axios from 'axios';
import { API_URL } from '../Constants/constants';
import { authorizationHeader, contentTypeHeader } from '../Utils/axiosHeaders';
import jwt from 'jsonwebtoken';
import { loginData, registerData } from '../Utils/axiosData'

export const loginUser = (username, password, setAuth, setLoginError, navigate) => {
  axios
          .post(API_URL + '/users/login', loginData(username, password), { headers: contentTypeHeader })
          .then((res) => {
            const user = jwt.decode(res.data.token);
            localStorage.setItem('token', res.data.token);
            setAuth({
              user,
              isLoggedIn: true,
            });
          })
          .then(() => navigate('/'))
          .catch((err) => setLoginError(err.response.data));
}

export const registerUser = (username, password, nickname, setRegisterError, navigate) => {
  axios
        .post(API_URL + '/users', registerData(username, password, nickname), { headers: contentTypeHeader })
        .then(() => navigate('/login'))
        .catch((err) => setRegisterError(err.response.data.message));
}

export const getCocktail = (setCocktail, cocktailId) => {
  axios
    .get(API_URL + `/cocktails/${cocktailId}`)
    .then((res) => setCocktail(res.data))
    .catch((err) => console.log(err.message));
};

export const getAllCocktails = (setCocktails) => {
  axios
    .get(API_URL + '/cocktails')
    .then((res) => {
      setCocktails(res.data);
    })
    .catch((err) => console.log(err.message));
};

export const addNewCocktail = (data) => {
  axios
    .post(API_URL + '/cocktails', data, { headers: authorizationHeader })
    .catch((err) => console.log(err));
};

export const editCocktailName = (currentCocktailId, cocktailName) => {
  axios
    .put(
      API_URL + `/cocktails/${currentCocktailId}`,
      { name: cocktailName },
      { headers: authorizationHeader }
    )
    .catch((err) => console.log(err.message));
};

export const editCocktailImage = (currentCocktailId, fd) => {
  axios
    .put(API_URL + `/cocktails/${currentCocktailId}`, fd, {
      headers: { ...authorizationHeader, ...contentTypeHeader },
    })
    .catch((err) => console.log(err.message));
};

export const addIngredientsToCocktail = (cocktailId, checked) => {
  axios.put(
    API_URL + `/cocktails/${cocktailId}/ingredients`,
    { ingredients: checked.map((i) => i.name) },
    {
      headers: { ...authorizationHeader, ...contentTypeHeader },
    }
  );
};

export const deleteIngredientFromCocktail = (
  currentCocktailId,
  ingredientToDelete
) => {
  axios
    .delete(API_URL + `/cocktails/${currentCocktailId}/ingredients`, {
      data: { ingredients: ingredientToDelete && [ingredientToDelete] },
      headers: authorizationHeader,
    })
    .catch((err) => console.log(err.message));
};

export const getIngredients = (setIngredients) => {
  axios
    .get(API_URL + '/ingredients', { headers: authorizationHeader })
    .then((res) => setIngredients(res.data))
    .catch((err) => console.log(err.message));
};

export const deleteIngredient = (ingredientID) => {
  axios
    .delete(API_URL + `/ingredients/${ingredientID}`, {
      headers: authorizationHeader,
    })
    .catch((err) => console.log(err.message));
};

export const addIngredient = (ingredientName) => {
  axios
    .post(
      API_URL + '/ingredients',
      { name: ingredientName },
      {
        headers: { ...authorizationHeader, ...contentTypeHeader },
      }
    )
    .catch((err) => console.log(err.message));
};
