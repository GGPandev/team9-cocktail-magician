import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './Pages/Home/Home';
import Bars from './Pages/Bars/Bars';
import Login from './Pages/Login/Login';
import { AuthContext, getUser } from './Providers/AuthContext';
import { useState } from 'react';
import Register from './Pages/Register/Register';
import Cocktails from './Pages/Cocktails/Cocktails';
import Ingredients from './Pages/Ingredients/Ingredients';
import CocktailsDetails from './Pages/CocktailsDetails/CocktailsDetails';
import CocktailID from './Providers/CocktailID';
import BarDetails from './Pages/BarDetails/BarDetails';
import BarContext from './Providers/BarContext';
import UserDetails from './Pages/UserDetails/UserDetails';
import UserContext from './Providers/UserContext';
import CocktailsTable from './Pages/CocktailsAdminTable/CocktailsTable';
import Header from './Components/Header/Header';
import AddBar from './Pages/AddBar/AddBar';
import ProfileInfo from './Pages/ProfileInfo/ProfileInfo';
import Footer from './Components/Footer/Footer';

function App() {
  const [auth, setAuth] = useState({
    user: getUser(),
    isLoggedIn: !!getUser(),
  });

  const [cocktailId, setCocktailID] = useState({ cocktailId: 0 });
  const [barStore, setBarStore] = useState({ id: 0 });
  const [userId, setUserId] = useState({ userId: 0 });

  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      <CocktailID.Provider value={{ cocktailId, setCocktailID }}>
        <BarContext.Provider value={{ barStore, setBarStore }}>
          <UserContext.Provider value={{ userId, setUserId }}>
            <BrowserRouter>
              <Header />
              <div className="App">
                <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/bars" element={<Bars />} />
                  <Route path="/bar-details" element={<BarDetails />} />
                  <Route path="/user-details" element={<UserDetails />} />
                  <Route
                    path="/user-profile-details"
                    element={<ProfileInfo />}
                  />
                  <Route path="/cocktails" element={<Cocktails />} />
                  <Route path="/add-bar" element={<AddBar />} />
                  <Route
                    path="/cocktails-details"
                    element={<CocktailsDetails />}
                  />
                  <Route path="/admin/ingredients" element={<Ingredients />} />
                  <Route path="/admin/cocktails" element={<CocktailsTable />} />
                </Routes>
              </div>
              <Footer />
            </BrowserRouter>
          </UserContext.Provider>
        </BarContext.Provider>
      </CocktailID.Provider>
    </AuthContext.Provider>
  );
}

export default App;
