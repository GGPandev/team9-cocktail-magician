export const logout = ( setAuth ) => {
  localStorage.removeItem('token');
  setAuth({
    user: null,
    isLoggedIn: false
  })
};